﻿using System.Text.Json.Serialization;

namespace CodeZero.Infrastructure.ClientMetaInfo
{
    public class ClientInfo
    {
        [JsonPropertyName("ip")]
        public string Ip { get; set; }

        [JsonPropertyName("countryName")]
        public string CountryName { get; set; }

        [JsonPropertyName("city")]
        public string City { get; set; }

        [JsonPropertyName("osPlatform")]
        public string OSPlatform { get; set; }

        [JsonPropertyName("browser")]
        public string Browser { get; set; }
    }
}