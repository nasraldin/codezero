﻿using CodeZero.Configuration;
using System;
using System.IO;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace CodeZero.Infrastructure.ClientMetaInfo
{
    public class LocationRequest
    {
        public static async Task<ClientGeolocationInfo> ClientGeoInfo(string ipAddress = "")
        {
            var config = CoreConfiguration.Instance.LocationApi;
            string clientIP = ipAddress.IsNullOrEmpty() ? CodeZeroHelper.GetClientIPAddress() : ipAddress;
            var clientIPAddress = IPAddress.Parse(clientIP);
            string url = $"{config.Url.EnsureEndsWith('/')}{clientIPAddress}?access_key={config.AccessKey}";
            var request = WebRequest.Create(url);

            using WebResponse wrs = await request.GetResponseAsync();
            using Stream stream = wrs.GetResponseStream();
            using StreamReader reader = new(stream);

            return reader.ReadToEnd().FromJson<ClientGeolocationInfo>();
        }
    }
}