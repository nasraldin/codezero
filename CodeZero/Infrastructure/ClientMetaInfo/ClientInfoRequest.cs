﻿using System.Threading.Tasks;

namespace CodeZero.Infrastructure.ClientMetaInfo
{
    public class ClientInfoRequest
    {
        public static async Task<ClientInfo> ClientInfo(string ipAddress)
        {
            var geo = await LocationRequest.ClientGeoInfo(ipAddress);

            return new ClientInfo
            {
                Ip = geo.Ip,
                City = geo.City,
                CountryName = geo.CountryName,
                OSPlatform = CodeZeroHelper.GetUserPlatform(),
                Browser = CodeZeroHelper.GetUserBrowser()
            };
        }
    }
}