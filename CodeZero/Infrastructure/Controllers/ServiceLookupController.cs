﻿using CodeZero.Common.Models;
using CodeZero.Data;
using CodeZero.Data.Lookup;
using CodeZero.Data.Query;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using static CodeZero.Data.Lookup.FilterationUtils;

namespace CodeZero.Infrastructure.Controllers
{
    public abstract class ServiceLookupController<T> : ControllerBase
    {
        private readonly IServiceLookupManager<T> _manager;

        public ServiceLookupController(IServiceLookupManager<T> manager)
        {
            _manager = manager;
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] LookupFilter filterDto)
        {
            LookupOutput response;

            if (IsFilterProvided(filterDto?.ValueToLookup))
            {
                var results = LoadSingleLookupData(filterDto);
                response = FormulateResponse(results);
            }
            else if (IsAnyFilterProvided(filterDto?.Skip, filterDto?.Limit, filterDto?.PageNumber))
            {
                var result = LoadLookupDataFiltered(filterDto);
                var resultItems = result?.Items;

                response = FormulateResponse(resultItems);
                AppendPagingMetadata(filterDto, response, result);
            }
            else
            {
                var resultItems = LoadLookupData();
                resultItems = resultItems.Where(Filter(filterDto));

                response = FormulateResponse(resultItems);
            }

            return Ok(response);
        }

        protected abstract Lookup MaptoLookupDto(T entity);

        /// <summary>
        /// Filteration when there are not pagination option are given. 
        /// It runs on the collection that is fetched from the database by loaders.
        /// Kept for historical usages.
        /// </summary>
        protected abstract Func<T, bool> Filter(LookupFilter filterDto);

        #region Overridable Loaders

        protected virtual IEnumerable<T> LoadLookupData()
        {
            //return _manager.GetAll();
            return null;
        }

        protected virtual PagedResult<T> LoadLookupDataFiltered(LookupFilter filterDto)
        {
            return _manager.GetFiltered($"1 = 1", new { }, filterDto);
        }

        protected virtual IEnumerable<T> LoadSingleLookupData(LookupFilter filterDto)
        {
            var valuesIds = filterDto.ValueToLookup.Split(",");
            if (valuesIds.Length > 1)
            {
                return _manager.GetSpecific($"Id IN @ids", new { ids = valuesIds.ToList<object>() });
            }
            else
            {
                return _manager.GetSpecific($"Id = @id", new { id = filterDto.ValueToLookup });
            }
        }

        #endregion

        #region Helpers

        private LookupOutput FormulateResponse(IEnumerable<T> list)
        {
            var response = new LookupOutput() { Values = new List<Lookup>() };

            foreach (var item in list)
            {
                if (item != null)
                    response.Values.Add(MaptoLookupDto(item));
            }

            return response;
        }

        private void AppendPagingMetadata(LookupFilter filterDto, LookupOutput response, PagedResult<T> result)
        {
            response.TotalCount = result.PageInfo.Count;
            if (!string.IsNullOrEmpty(filterDto.PageNumber))
            {
                int.TryParse(filterDto.PageNumber, out int currentPage);
                response.CurrentPage = currentPage;
            }
        }

        #endregion
    }
}