﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace CodeZero.Infrastructure.Controllers
{
    public class MetaController : AnonymousController
    {
        [HttpGet("Info")]
        public ActionResult<string> Info()
        {
            var assembly = typeof(MetaController).Assembly;

            var creationDate = System.IO.File.GetCreationTime(assembly.Location);
            var version = FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;

            return Ok($"Version: {version}, Last Updated: {creationDate}");
        }
    }
}