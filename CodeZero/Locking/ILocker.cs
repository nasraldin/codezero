using System;

namespace CodeZero.Locking
{
    public interface ILocker : IDisposable, IAsyncDisposable { }
}