﻿using System.Threading.Tasks;

namespace CodeZero.Sms
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}