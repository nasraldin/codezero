using CodeZero.Configuration;
using CodeZero.Utils;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// Registration of the dependency in a service container.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add Reverse Proxy.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddProxy([NotNull] this IServiceCollection services)
        {
            var proxySettings = CoreConfiguration.Instance.ProxySettings;
            if (proxySettings.ForwardedHeadersOptions != null)
            {
                // Configure ASP.NET Core to work with proxy servers and load balancers
                services.Configure<ForwardedHeadersOptions>(options =>
                {
                    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;

                    if (proxySettings.ForwardedHeadersOptions.ForwardLimit.HasValue)
                        options.ForwardLimit = proxySettings.ForwardedHeadersOptions.ForwardLimit;

                    if (proxySettings.ForwardedHeadersOptions.ForwardedForHeaderName.IsNotNullOrEmpty())
                        options.ForwardedForHeaderName = proxySettings.ForwardedHeadersOptions.ForwardedForHeaderName;

                    if (proxySettings.ForwardedHeadersOptions.ForwardedHostHeaderName.IsNotNullOrEmpty())
                        options.ForwardedHostHeaderName = proxySettings.ForwardedHeadersOptions.ForwardedHostHeaderName;

                    if (proxySettings.ForwardedHeadersOptions.ForwardedProtoHeaderName.IsNotNullOrEmpty())
                        options.ForwardedProtoHeaderName = proxySettings.ForwardedHeadersOptions.ForwardedProtoHeaderName;

                    if (proxySettings.ForwardedHeadersOptions.OriginalForHeaderName.IsNotNullOrEmpty())
                        options.OriginalForHeaderName = proxySettings.ForwardedHeadersOptions.OriginalForHeaderName;

                    if (proxySettings.ForwardedHeadersOptions.OriginalHostHeaderName.IsNotNullOrEmpty())
                        options.OriginalHostHeaderName = proxySettings.ForwardedHeadersOptions.OriginalHostHeaderName;

                    if (proxySettings.ForwardedHeadersOptions.OriginalProtoHeaderName.IsNotNullOrEmpty())
                        options.OriginalProtoHeaderName = proxySettings.ForwardedHeadersOptions.OriginalProtoHeaderName;

                    // Only loopback proxies are allowed by default.
                    // Clear that restriction because forwarders are enabled by explicit configuration.
                    options.KnownNetworks.Clear();
                    options.KnownProxies.Clear();

                    if (proxySettings.ForwardedHeadersOptions.KnownProxies.Any())
                    {
                        proxySettings.ForwardedHeadersOptions.KnownProxies.ForEach(item =>
                        {
                            options.KnownProxies.Add(IPAddress.Parse(item));
                        });
                    }

                    if (proxySettings.ForwardedHeadersOptions.AddActiveNetworkInterfaceToKnownNetworks)
                    {
                        foreach (var network in Network.GetNetworks(NetworkInterfaceType.Ethernet))
                        {
                            options.KnownNetworks.Add(network);
                        }
                    }

                    if (proxySettings.ForwardedHeadersOptions.RequireHeaderSymmetry)
                        options.RequireHeaderSymmetry = proxySettings.ForwardedHeadersOptions.RequireHeaderSymmetry;
                });
            }

            if (proxySettings.HstsOptions != null)
            {
                // Enforce HTTPS
                services.AddHsts(options =>
                {
                    if (proxySettings.HstsOptions.Preload)
                        options.Preload = proxySettings.HstsOptions.Preload;

                    if (proxySettings.HstsOptions.IncludeSubDomains)
                        options.IncludeSubDomains = proxySettings.HstsOptions.IncludeSubDomains;

                    if (!proxySettings.HstsOptions.MaxAge.Equals(0))
                        options.MaxAge = TimeSpan.FromDays(proxySettings.HstsOptions.MaxAge);

                    if (proxySettings.HstsOptions.ExcludedHosts.Any())
                    {
                        proxySettings.HstsOptions.ExcludedHosts.ForEach(item =>
                        {
                            options.ExcludedHosts.Add(item);
                        });
                    }
                });
            }

            if (proxySettings.HttpsRedirectionOptions != null)
            {
                // Configure temporary/permanent redirects in production (Status308PermanentRedirect)
                services.AddHttpsRedirection(options =>
                {
                    if (!proxySettings.HttpsRedirectionOptions.RedirectStatusCode.Equals(0))
                        options.RedirectStatusCode = proxySettings.HttpsRedirectionOptions.RedirectStatusCode;

                    if (proxySettings.HttpsRedirectionOptions.HttpsPort.HasValue)
                        options.HttpsPort = proxySettings.HttpsRedirectionOptions.HttpsPort.Value;
                });
            }

            return services;
        }
    }
}

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Configure HTTP request pipeline for the current path.
    /// </summary>
    public static partial class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Register Reverse Proxy.
        /// </summary>
        /// <param name="app">The <see cref="IApplicationBuilder"/>.</param>
        /// <returns><see cref="IApplicationBuilder"/></returns>
        public static IApplicationBuilder UseProxy([NotNull] this IApplicationBuilder app)
        {
            var proxySettings = CoreConfiguration.Instance.ProxySettings;

            if (proxySettings.RequestBasePath.IsNotNullOrEmpty())
            {
                app.Use(async (context, next) =>
                {
                    context.Request.PathBase = proxySettings.RequestBasePath;
                    await next.Invoke().ConfigureAwait(false);
                });
            }

            var forwardedOptions = new ForwardedHeadersOptions()
            {
                ForwardedHeaders = ForwardedHeaders.All
            };

            if (proxySettings.ForwardedHeadersOptions.AddActiveNetworkInterfaceToKnownNetworks)
            {
                foreach (var network in Network.GetNetworks(NetworkInterfaceType.Ethernet))
                {
                    forwardedOptions.KnownNetworks.Add(network);
                }
            }

            app.UseForwardedHeaders(forwardedOptions);

            // The default HSTS value is 30 days.
            // You may want to change this for production scenarios,
            // see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
            app.UseHttpsRedirection();

            return app;
        }
    }
}