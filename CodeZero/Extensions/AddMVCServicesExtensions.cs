using CodeZero;
using CodeZero.Configuration;
using CodeZero.Feature;
using FluentValidation.AspNetCore;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// Registration of the dependency in a service container.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds MVC services.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddMvcServices([NotNull] this IServiceCollection services)
        {
            services.AddRouting(x => x.LowercaseUrls = CoreConfiguration.Instance.ApiSettings.RoutingLowercaseUrls);

            services.AddMvc(mvcOptions =>
            {
                //mvcOptions.OutputFormatters.RemoveType<TextOutputFormatter>();
                //mvcOptions.OutputFormatters.RemoveType<StreamOutputFormatter>();
                //mvcOptions.RespectBrowserAcceptHeader = true;
            })
            .AddFluentValidation(fvc =>
            {
                fvc.RegisterValidatorsFromAssemblyContaining<Startup>();
            })
            .SetCompatibilityVersion(CompatibilityVersion.Latest)
            .AddJsonOptions(jsonOptions =>
            {
                jsonOptions.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
                jsonOptions.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                jsonOptions.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
                jsonOptions.JsonSerializerOptions.IgnoreNullValues = true;
            })
            .ConfigureApplicationPartManager(apm => apm.FeatureProviders.Add(new CustomControllerFeatureProvider(AppSettings.Instance.FeatureManager)))
            .AddControllersAsServices();

            return services;
        }

        /// <summary>
        /// Adds Content Negotiation configure.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddContentNegotiation([NotNull] this IServiceCollection services)
        {
            services.AddControllers(config =>
            {
                // Add XML Content Negotiation
                config.RespectBrowserAcceptHeader = true;

                // tells the server that if the client tries to negotiate for the media type the server doesn�t support, it should return the 406 Not Acceptable status code.
                config.ReturnHttpNotAcceptable = true;

            }).AddJsonOptions(jsonOptions =>
            {
                jsonOptions.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
                jsonOptions.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                jsonOptions.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
                jsonOptions.JsonSerializerOptions.IgnoreNullValues = true;
            })
            .AddXmlSerializerFormatters()
            .AddXmlDataContractSerializerFormatters(); // support XML formatters

            return services;
        }
    }
}

//namespace Microsoft.AspNetCore.Builder
//{
//    /// <summary>
//    /// Configure HTTP request pipeline for the current path.
//    /// </summary>
//    public static partial class ApplicationBuilderExtensions
//    {
//        /// <summary>
//        /// Register MVC Services.
//        /// </summary>
//        /// <param name="app">The <see cref="IApplicationBuilder"/>.</param>
//        /// <returns><see cref="IApplicationBuilder"/></returns>
//        public static IApplicationBuilder UseMvcServices([NotNull] this IApplicationBuilder app)
//        {
//            return app.UseMvc();
//        }
//    }
//}