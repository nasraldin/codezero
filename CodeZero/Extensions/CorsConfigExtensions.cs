using CodeZero;
using CodeZero.Configuration;
using JetBrains.Annotations;
using System;
using System.Linq;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// Registration of the dependency in a service container.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds CORS.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddCorsConfig([NotNull] this IServiceCollection services)
        {
            var corsConfig = CoreConfiguration.Instance.CorsSettings;

            services.AddCors(options =>
            {
                if (corsConfig.CorsPolicy.Any())
                {
                    if (corsConfig.CorsPolicy.Any(cp => cp.PolicyName.IsNullOrEmpty()))
                        throw new CodeZeroException("PolicyName can't be null!");

                    corsConfig.CorsPolicy.ForEach(policy =>
                    {
                        options.AddPolicy(name: policy.PolicyName,
                            builder =>
                            {
                                // check for Cors allow specific
                                if (policy.AllowAnyHeader)
                                    builder.AllowAnyHeader();
                                if (policy.AllowAnyMethod)
                                    builder.AllowAnyMethod();
                                if (policy.AllowAnyOrigin)
                                    builder.AllowAnyOrigin();

                                // not any
                                if (!policy.AllowAnyHeader && policy.Headers != null)
                                    builder.WithHeaders(policy.Headers);

                                if (!policy.AllowAnyMethod && policy.Methods != null)
                                    builder.WithHeaders(policy.Methods);

                                if (!policy.AllowAnyOrigin && policy.Origins != null)
                                    builder.WithHeaders(policy.Origins);

                                if (policy.SupportsCredentials)
                                    builder.AllowCredentials();

                                if (policy.PreflightMaxAge.HasValue)
                                    builder.SetPreflightMaxAge(policy.PreflightMaxAge.Value).AllowCredentials();

                                //.SetIsOriginAllowed(s => Regex.IsMatch(s, "https://myExampleTenantName.*\\.sharepoint\\.com"));
                            });
                    });
                }
            });

            return services;
        }
    }
}

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Configure HTTP request pipeline for the current path.
    /// </summary>
    public static partial class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Enable CORS Configuration
        /// </summary>
        /// <param name="app">The <see cref="IApplicationBuilder"/>.</param>
        /// <returns><see cref="IApplicationBuilder"/></returns>
        public static IApplicationBuilder UseCorsConfig([NotNull] this IApplicationBuilder app)
        {
            if (CoreConfiguration.Instance.CorsSettings.DefaultCorsPolicy.IsNotNullOrEmpty())
                app.UseCors(CoreConfiguration.Instance.CorsSettings.DefaultCorsPolicy);

            return app;
        }
    }
}