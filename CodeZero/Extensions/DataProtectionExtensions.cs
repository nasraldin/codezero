using CodeZero.Configuration;
using JetBrains.Annotations;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.DataProtection.XmlEncryption;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.IO;
using System.Reflection;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// Registration of the dependency in a service container.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add DataProtection.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddDataProtectionConfig([NotNull] this IServiceCollection services)
        {
            var dpConfig = CoreConfiguration.Instance.DataProtectionConfig;

            // Remove any previously registered options setups.
            services.RemoveAll<IConfigureOptions<KeyManagementOptions>>();
            services.RemoveAll<IConfigureOptions<DataProtectionOptions>>();

            // The 'FileSystemXmlRepository' will create the directory, but only if it is not overridden.
            var directory = new DirectoryInfo(Path.Combine("AppData", dpConfig.FileSystemDirectoryName));

            // Adds app level data protection services.
            var builder = services.AddDataProtection().SetApplicationName(Assembly.GetExecutingAssembly().FullName);

            if (dpConfig.PersistKeysToRedis)
            {
                ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(CoreConfiguration.Instance.RedisConfig.ConnectionString);
                builder.PersistKeysToStackExchangeRedis(redis, dpConfig.RedisKey);
            }
            else
            {
                builder.PersistKeysToFileSystem(directory)
                .AddKeyManagementOptions(options =>
                {
                    options.NewKeyLifetime = TimeSpan.FromDays(dpConfig.KeyManagement.NewKeyLifetime);
                    options.AutoGenerateKeys = dpConfig.KeyManagement.AutoGenerateKeys;
                    options.XmlEncryptor ??= new NullXmlEncryptor();
                });
            }

            return services;
        }
    }
}