using CodeZero.Configuration;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// Registration of the dependency in a service container.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds Authentication.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddAuth([NotNull] this IServiceCollection services)
        {
            var authConfig = CoreConfiguration.Instance.Authentication;

            // Add AuthorizeFilter to all actions
            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();

                options.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(jwtOptions =>
            {
                jwtOptions.Authority = authConfig.Authority;
                jwtOptions.Audience = authConfig.Audience;
                jwtOptions.SaveToken = authConfig.SaveToken;

                jwtOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,

                    ValidIssuer = authConfig.Authority,
                    ValidAudience = authConfig.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authConfig.ClientSecret)),
                };
            });


            //    .AddOpenIdConnect(jwtOptions =>
            //{
            //    jwtOptions.Authority = authConfig.Authority;
            //    jwtOptions.ClientId = authConfig.ClientId;
            //    jwtOptions.ClientSecret = authConfig.ClientSecret;
            //    jwtOptions.SaveTokens = authConfig.SaveToken;
            //    jwtOptions.ResponseType = OpenIdConnectResponseType.Code;
            //    jwtOptions.RequireHttpsMetadata = false; // dev only
            //    jwtOptions.GetClaimsFromUserInfoEndpoint = true;

            //    //jwtOptions.Scope.Add("openid");
            //    //jwtOptions.Scope.Add("profile");
            //    //jwtOptions.Scope.Add("email");
            //    //jwtOptions.Scope.Add("claims");

            //    jwtOptions.TokenValidationParameters = new TokenValidationParameters()
            //    {
            //        ValidateIssuer = true,
            //        ValidIssuer = authConfig.Authority,
            //        ValidateAudience = true,
            //        ValidAudience = authConfig.Audience,
            //        ValidateLifetime = true,
            //        RequireExpirationTime = true,
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authConfig.ClientSecret)),
            //    };
            //});

            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)

            services.AddAuthorization();

            // IAuthenticationSchemeProvider is already registered at the host level.
            // We need to register it again so it is taken into account at the service level
            // because it holds a reference to an underlying dictionary, responsible of storing
            // the registered schemes which need to be distinct for each service.
            services.AddSingleton<IAuthenticationSchemeProvider, AuthenticationSchemeProvider>();

            return services;
        }
    }
}

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Configure HTTP request pipeline for the current path.
    /// </summary>
    public static partial class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Register the Auth Configuration
        /// </summary>
        /// <param name="app">The <see cref="IApplicationBuilder"/>.</param>
        /// <returns><see cref="IApplicationBuilder"/></returns>
        public static IApplicationBuilder UseAuth([NotNull] this IApplicationBuilder app)
        {
            // Adds authenticaton middleware to the pipeline 
            // so authentication will be performed automatically on each request to host
            app.UseAuthentication();

            // Adds authorization middleware to the pipeline 
            // to make sure the Api endpoint cannot be accessed by anonymous clients
            app.UseAuthorization();

            return app;
        }
    }
}