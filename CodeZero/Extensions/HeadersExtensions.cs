using CodeZero;
using CodeZero.Configuration;
using JetBrains.Annotations;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Configure HTTP request pipeline for the current path.
    /// </summary>
    public static partial class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Register the Headers
        /// </summary>
        /// <param name="app">The <see cref="IApplicationBuilder"/>.</param>
        /// <returns><see cref="IApplicationBuilder"/></returns>
        public static IApplicationBuilder UseHeaders([NotNull] this IApplicationBuilder app)
        {
            var headersConfig = CoreConfiguration.Instance.HeadersConfig;

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add(AppConsts.HeaderName.XFrameOptions, headersConfig.XFrameOptions);
                context.Response.Headers.Add(AppConsts.HeaderName.XssProtection, headersConfig.XssProtection);
                context.Response.Headers.Add(AppConsts.HeaderName.XContentTypeOptions, headersConfig.XContentTypeOptions);

                // Remove version discloser
                context.Response.Headers.Remove("X-AspNet-Version");
                context.Response.Headers.Remove("X-AspNetMvc-Version");
                context.Response.Headers.Remove("Server");


                if (!context.Response.HasStarted)
                    await next.Invoke();
                //await next();
            });

            return app;
        }
    }
}