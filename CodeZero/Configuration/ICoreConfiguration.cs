﻿using System.Collections.Generic;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents the app settings
    /// </summary>
    public interface ICoreConfiguration
    {
        string ApplicationName { get; set; }
        DebugConfig DebugConfig { get; set; }
        ConnectionStrings ConnectionStrings { get; set; }
        ApiSettings ApiSettings { get; set; }
        ApiKeyConfig ApiKeyConfig { get; set; }
        Authentication Authentication { get; set; }
        FeatureManagement FeatureManagement { get; set; }
        CacheConfig CacheConfig { get; set; }
        RedisConfig RedisConfig { get; set; }
        SeqOptions SeqOptions { get; set; }
        PaginationOptions PaginationOptions { get; set; }
        ICollection<ServiceEndpoints> ServiceEndpoints { get; set; }
        ICollection<HostedServices> HostedServices { get; set; }
        ApiMedia ApiMedia { get; set; }
        SwaggerInfo SwaggerInfo { get; set; }
        SwaggerConfig SwaggerConfig { get; set; }
        CorsSettings CorsSettings { get; set; }
        IpRateLimiting IpRateLimiting { get; set; }
        IpRateLimitPolicies IpRateLimitPolicies { get; set; }
        ICollection<Languages> Languages { get; set; }
        ProxySettings ProxySettings { get; set; }
        HeadersConfig HeadersConfig { get; set; }
        LocationApi LocationApi { get; set; }
        HealthChecksUI HealthChecksUI { get; set; }
        DataProtectionConfig DataProtectionConfig { get; set; }
        CommonConfig CommonConfig { get; set; }
    }
}