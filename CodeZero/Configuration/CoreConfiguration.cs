﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace CodeZero.Configuration
{
    /// <summary>
    /// We use the ICoreConfiguration type as a way to inject appSettings
    /// and resource connections into our classes from our main entry points.
    /// </summary>
    public class CoreConfiguration : ICoreConfiguration
    {
        #region Ctor
        public CoreConfiguration(IConfiguration configuration)
        {
            ApplicationName = configuration[nameof(ApplicationName)];
            DebugConfig = configuration.GetSection(nameof(DebugConfig)).Get<DebugConfig>();
            ApiSettings = configuration.GetSection(nameof(ApiSettings)).Get<ApiSettings>();
            ConnectionStrings = configuration.GetSection(nameof(ConnectionStrings)).Get<ConnectionStrings>();
            ApiKeyConfig = configuration.GetSection(nameof(ApiKeyConfig)).Get<ApiKeyConfig>();
            Authentication = configuration.GetSection(nameof(Authentication)).Get<Authentication>();
            FeatureManagement = configuration.GetSection(nameof(FeatureManagement)).Get<FeatureManagement>();
            ServiceEndpoints = configuration.GetSection(nameof(ServiceEndpoints)).Get<ICollection<ServiceEndpoints>>();
            HostedServices = configuration.GetSection(nameof(HostedServices)).Get<ICollection<HostedServices>>();
            SwaggerInfo = configuration.GetSection(nameof(SwaggerInfo)).Get<SwaggerInfo>();
            SwaggerConfig = configuration.GetSection(nameof(SwaggerConfig)).Get<SwaggerConfig>();
            CorsSettings = configuration.GetSection(nameof(CorsSettings)).Get<CorsSettings>();
            IpRateLimiting = configuration.GetSection(nameof(IpRateLimiting)).Get<IpRateLimiting>();
            IpRateLimitPolicies = configuration.GetSection(nameof(IpRateLimiting)).Get<IpRateLimitPolicies>();
            Languages = configuration.GetSection(nameof(Languages)).Get<ICollection<Languages>>();
            ProxySettings = configuration.GetSection(nameof(ProxySettings)).Get<ProxySettings>();
            HeadersConfig = configuration.GetSection(nameof(HeadersConfig)).Get<HeadersConfig>();
            LocationApi = configuration.GetSection(nameof(LocationApi)).Get<LocationApi>();
            CacheConfig = configuration.GetSection(nameof(CacheConfig)).Get<CacheConfig>();
            RedisConfig = configuration.GetSection(nameof(RedisConfig)).Get<RedisConfig>();
            SeqOptions = configuration.GetSection(nameof(SeqOptions)).Get<SeqOptions>();
            PaginationOptions = configuration.GetSection(nameof(PaginationOptions)).Get<PaginationOptions>();
            ApiMedia = configuration.GetSection(nameof(ApiMedia)).Get<ApiMedia>();
            HealthChecksUI = configuration.GetSection(nameof(HealthChecksUI)).Get<HealthChecksUI>();
            DataProtectionConfig = configuration.GetSection(nameof(DataProtectionConfig)).Get<DataProtectionConfig>();
            CommonConfig = configuration.GetSection(nameof(CommonConfig)).Get<CommonConfig>();

            Instance = this;
        }
        #endregion

        #region Props
        public string ApplicationName { get; set; }
        public DebugConfig DebugConfig { get; set; } = new DebugConfig();
        public ApiSettings ApiSettings { get; set; } = new ApiSettings();
        public ConnectionStrings ConnectionStrings { get; set; }
        public ApiKeyConfig ApiKeyConfig { get; set; } = new ApiKeyConfig();
        public Authentication Authentication { get; set; } = new Authentication();
        public FeatureManagement FeatureManagement { get; set; } = new FeatureManagement();
        public ICollection<ServiceEndpoints> ServiceEndpoints { get; set; }
        public ICollection<HostedServices> HostedServices { get; set; }
        public SwaggerInfo SwaggerInfo { get; set; } = new SwaggerInfo();
        public SwaggerConfig SwaggerConfig { get; set; } = new SwaggerConfig();
        public CorsSettings CorsSettings { get; set; } = new CorsSettings();
        public IpRateLimiting IpRateLimiting { get; set; } = new IpRateLimiting();
        public IpRateLimitPolicies IpRateLimitPolicies { get; set; } = new IpRateLimitPolicies();
        public ICollection<Languages> Languages { get; set; }
        public ProxySettings ProxySettings { get; set; }
        public HeadersConfig HeadersConfig { get; set; }
        public LocationApi LocationApi { get; set; }
        public CacheConfig CacheConfig { get; set; }
        public RedisConfig RedisConfig { get; set; }
        public SeqOptions SeqOptions { get; set; }
        public PaginationOptions PaginationOptions { get; set; }
        public ApiMedia ApiMedia { get; set; }
        public HealthChecksUI HealthChecksUI { get; set; }
        public DataProtectionConfig DataProtectionConfig { get; set; }
        public CommonConfig CommonConfig { get; set; }
        #endregion

        public static CoreConfiguration Instance;
    }
}