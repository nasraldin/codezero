﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.FeatureManagement;
using System.Collections.Generic;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents the app settings
    /// </summary>
    public interface IAppSettings
    {
        string ApplicationName { get; set; }
        DebugConfig DebugConfig { get; set; }
        ApiSettings ApiSettings { get; set; }
        ApiKeyConfig ApiKeyConfig { get; set; }
        FeatureManagement FeatureManagement { get; set; }
        ICollection<ServiceEndpoints> ServiceEndpoints { get; set; }
        ICollection<HostedServices> HostedServices { get; set; }

        IWebHostEnvironment Env { get; set; }
        IConfiguration Configuration { get; set; }
        IFeatureManager FeatureManager { get; set; }

        string Version { get; }
        string MachineName { get; }
        string OSVersion { get; }
        string OSDescription { get; }
        string NetCoreVersion { get; }
    }
}