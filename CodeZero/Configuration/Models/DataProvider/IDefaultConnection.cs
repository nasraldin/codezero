﻿namespace CodeZero.Configuration.Models.DataProvider
{
    public interface IDefaultConnection
    {
        string DefaultConnection { get; set; }
    }
}