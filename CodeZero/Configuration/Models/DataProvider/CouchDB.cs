﻿namespace CodeZero.Configuration.Models.DataProvider
{
    public partial class CouchDB : IDefaultConnection
    {
        public string DefaultConnection { get; set; }
    }
}