﻿namespace CodeZero.Configuration.Models.DataProvider
{
    public partial class CouchBase : IDefaultConnection
    {
        public string DefaultConnection { get; set; }
    }
}