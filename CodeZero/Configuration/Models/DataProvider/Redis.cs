﻿namespace CodeZero.Configuration.Models.DataProvider
{
    public partial class Redis : IDefaultConnection
    {
        public string DefaultConnection { get; set; }
    }
}