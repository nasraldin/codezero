﻿namespace CodeZero.Configuration.Models.DataProvider
{
    public partial class MariaDB : IDefaultConnection
    {
        public string DefaultConnection { get; set; }
    }
}