﻿namespace CodeZero.Configuration.Models.DataProvider
{
    public partial class MongoDB : IDefaultConnection
    {
        public string DefaultConnection { get; set; }
    }
}