﻿namespace CodeZero.Configuration.Models.DataProvider
{
    public partial class Cassandra : IDefaultConnection
    {
        public string DefaultConnection { get; set; }
    }
}