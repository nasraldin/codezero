﻿namespace CodeZero.Configuration.Models.DataProvider
{
    public partial class PostgreSQL : IDefaultConnection
    {
        public string DefaultConnection { get; set; }
    }
}