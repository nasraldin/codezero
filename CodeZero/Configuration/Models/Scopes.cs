﻿namespace CodeZero.Configuration.Models
{
    public partial class Scopes
    {
        public string ScopeName { get; set; }
        public string ShortDescription { get; set; }
    }
}