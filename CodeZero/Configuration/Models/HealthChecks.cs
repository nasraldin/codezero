﻿namespace CodeZero.Configuration.Models
{
    public partial class HealthChecks
    {
        public string Name { get; set; }
        public string Uri { get; set; }
    }
}