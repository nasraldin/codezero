﻿using System.Collections.Generic;

namespace CodeZero.Configuration.Models
{
    public partial class IpRules
    {
        public string Ip { get; set; }
        public List<GeneralRules> Rules { get; set; }
    }
}