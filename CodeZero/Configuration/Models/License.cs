﻿namespace CodeZero.Configuration.Models
{
    public partial class License
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}