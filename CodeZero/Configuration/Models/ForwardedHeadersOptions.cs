﻿namespace CodeZero.Configuration.Models
{
    public class ForwardedHeadersOptions
    {
        public string ForwardedForHeaderName { get; set; }
        public string ForwardedHostHeaderName { get; set; }
        public string ForwardedProtoHeaderName { get; set; }
        public string OriginalForHeaderName { get; set; }
        public string OriginalHostHeaderName { get; set; }
        public string OriginalProtoHeaderName { get; set; }
        public int? ForwardLimit { get; set; }
        public string[] KnownProxies { get; set; }
        public string[] KnownNetworks { get; set; }
        public string[] AllowedHosts { get; set; }
        public bool RequireHeaderSymmetry { get; set; }
        public bool AddActiveNetworkInterfaceToKnownNetworks { get; set; }
    }

    public class HstsOptions
    {
        public int MaxAge { get; set; }
        public bool IncludeSubDomains { get; set; }
        public bool Preload { get; set; }
        public string[] ExcludedHosts { get; set; }
    }

    public class HttpsRedirectionOptions
    {
        public int RedirectStatusCode { get; set; }
        public int? HttpsPort { get; set; }
    }
}