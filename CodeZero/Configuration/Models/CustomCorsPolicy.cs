﻿using System;
using System.Text.Json.Serialization;

namespace CodeZero.Configuration.Models
{
    public class CustomCorsPolicy
    {
        public string PolicyName { get; set; }
        public bool AllowAnyHeader { get; set; }
        public bool AllowAnyMethod { get; set; }
        public bool AllowAnyOrigin { get; set; }
        [JsonIgnore]
        public string[] Headers { get; set; }
        [JsonIgnore]
        public string[] Methods { get; set; }
        [JsonIgnore]
        public string[] Origins { get; set; }
        public TimeSpan? PreflightMaxAge { get; set; }
        public bool SupportsCredentials { get; set; }
    }
}