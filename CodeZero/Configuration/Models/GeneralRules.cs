﻿namespace CodeZero.Configuration.Models
{
    public partial class GeneralRules
    {
        public string Endpoint { get; set; }
        public string Period { get; set; }
        public double Limit { get; set; }
    }
}