﻿namespace CodeZero.Configuration.Models
{
    public partial class AllowedClients
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}