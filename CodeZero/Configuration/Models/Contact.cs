﻿namespace CodeZero.Configuration.Models
{
    public partial class Contact
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
    }
}