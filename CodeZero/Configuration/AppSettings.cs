﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.FeatureManagement;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents the most common app settings ared used in the application
    /// </summary>
    public class AppSettings : IAppSettings
    {
        public static AppSettings Instance;

        public AppSettings(IConfiguration configuration, IWebHostEnvironment env, IFeatureManager featureManager)
        {

            Configuration = configuration;
            Env = env;
            FeatureManager = featureManager;

            ApplicationName = configuration[nameof(ApplicationName)];
            DebugConfig = configuration.GetSection(nameof(DebugConfig)).Get<DebugConfig>();
            ApiSettings = configuration.GetSection(nameof(ApiSettings)).Get<ApiSettings>();
            ApiKeyConfig = configuration.GetSection(nameof(ApiKeyConfig)).Get<ApiKeyConfig>();
            FeatureManagement = configuration.GetSection(nameof(FeatureManagement)).Get<FeatureManagement>();
            ServiceEndpoints = configuration.GetSection(nameof(ServiceEndpoints)).Get<ICollection<ServiceEndpoints>>();
            HostedServices = configuration.GetSection(nameof(HostedServices)).Get<ICollection<HostedServices>>();

            Instance = this;
        }

        public string ApplicationName { get; set; }
        public DebugConfig DebugConfig { get; set; } = new DebugConfig();
        public ApiSettings ApiSettings { get; set; } = new ApiSettings();
        public ApiKeyConfig ApiKeyConfig { get; set; } = new ApiKeyConfig();
        public FeatureManagement FeatureManagement { get; set; } = new FeatureManagement();
        public ICollection<ServiceEndpoints> ServiceEndpoints { get; set; }
        public ICollection<HostedServices> HostedServices { get; set; }

        public IWebHostEnvironment Env { get; set; }
        public IConfiguration Configuration { get; set; }
        public IFeatureManager FeatureManager { get; set; }

        #region System only overwritable and read-only settings

        public string Version
        {
            get
            {
                return typeof(AppSettings)
                    .GetTypeInfo()
                    .Assembly
                    .GetCustomAttribute<AssemblyInformationalVersionAttribute>()
                    .InformationalVersion;
            }
        }

        public string MachineName
        {
            get
            {
                return Environment.MachineName;
            }
        }

        public string OSVersion
        {
            get
            {
                return Environment.OSVersion?.VersionString;
            }
        }

        public string OSDescription
        {
            get
            {
                return System.Runtime.InteropServices.RuntimeInformation.OSDescription;
            }
        }

        public string NetCoreVersion
        {
            get
            {
                return System.Runtime.InteropServices.RuntimeInformation.FrameworkDescription;
            }
        }

        #endregion
    }
}