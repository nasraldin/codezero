﻿namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents HostedServices configuration parameters
    /// </summary>
    public partial class HostedServices
    {
        public string HostedServiceName { get; set; }
        public string HostedServiceClientSecret { get; set; }
        public string HostedServiceRequestedScopes { get; set; }
        public int HostedServiceInterval { get; set; } = 5000;
    }
}