﻿using CodeZero.Configuration.Models.DataProvider;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents ConnectionStrings configuration parameters
    /// </summary>
    public partial class ConnectionStrings
    {
        public SqlServer SqlServer { get; set; }
        public MySql MySql { get; set; }
        public MariaDB MariaDB { get; set; }
        public PostgreSQL PostgreSQL { get; set; }
        public MongoDB MongoDB { get; set; }
        public Cassandra Cassandra { get; set; }
        public CouchDB CouchDB { get; set; }
        public CouchBase CouchBase { get; set; }
        public Models.DataProvider.Redis Redis { get; set; }
    }
}