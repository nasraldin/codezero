﻿namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents Languages configuration parameters
    /// </summary>
    public partial class Languages
    {
        public string Name { get; set; }
        public string Culture { get; set; }
        public bool IsRtl { get; set; }
        public bool IsActive { get; set; }
        public int DisplayOrder { get; set; }
    }
}