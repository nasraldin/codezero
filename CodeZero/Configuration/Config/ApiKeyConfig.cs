﻿using CodeZero.Configuration.Models;
using System.Collections.Generic;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents ApiKey configuration parameters
    /// </summary>
    public partial class ApiKeyConfig
    {
        public bool EnableApiKey { get; set; } = false;
        public string ApiKey { get; set; }
        public string HeaderName { get; set; }
        public List<AllowedClients> AllowedClients { get; set; }
    }
}