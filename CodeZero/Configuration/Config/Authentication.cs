﻿namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents Authentication configuration parameters
    /// </summary>
    public partial class Authentication
    {
        public string Authority { get; set; }
        public string Audience { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Scopes { get; set; }
        public bool SaveToken { get; set; }
    }
}