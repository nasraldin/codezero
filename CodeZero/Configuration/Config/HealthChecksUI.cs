﻿using CodeZero.Configuration.Models;
using System.Collections.Generic;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents HealthChecksUI configuration parameters
    /// </summary>
    public partial class HealthChecksUI
    {
        public string UiEndpoint { get; set; } = "healthchecks-ui";
        public string HeaderText { get; set; } = "CodeZero - Health Checks Status";
        public string StorageConnectionString { get; set; }
        public bool EnableDatabaseStorage { get; set; } = false;
        public List<HealthChecks> HealthChecks { get; set; }
    }
}