﻿namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents ServiceEndpoints configuration parameters
    /// </summary>
    public partial class ServiceEndpoints
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string ApiKey { get; set; }
    }
}