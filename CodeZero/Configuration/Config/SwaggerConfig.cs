﻿using CodeZero.Configuration.Models;
using System.Collections.Generic;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents Swagger configuration parameters
    /// </summary>
    public partial class SwaggerConfig
    {
        public string RouteTemplate { get; set; } = "swagger/{documentName}/swagger.json";
        public string RoutePrefix { get; set; } = "swagger.json";
        public string UiEndpoint { get; set; } = "swagger";
        public string AuthorizationUrl { get; set; }
        public List<Scopes> Scopes { get; set; }
        public int DefaultModelsExpandDepth { get; set; } = 1;
    }
}