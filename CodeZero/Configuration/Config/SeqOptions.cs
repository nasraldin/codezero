﻿namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents Seq configuration parameters
    /// </summary>
    public partial class SeqOptions
    {
        public string Endpoint { get; set; }
        public string ApiKey { get; set; }
    }
}