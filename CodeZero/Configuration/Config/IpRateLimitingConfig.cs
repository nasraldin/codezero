﻿using CodeZero.Configuration.Models;
using System.Collections.Generic;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents IpRateLimiting configuration parameters
    /// </summary>
    public partial class IpRateLimiting
    {
        public bool EnableEndpointRateLimiting { get; set; } = false;
        public bool StackBlockedRequests { get; set; } = false;
        public string RealIpHeader { get; set; } = "X-Real-IP";
        public string ClientIdHeader { get; set; } = "X-ClientId";
        public int HttpStatusCode { get; set; } = 429;
        public List<string> IpWhitelist { get; set; }
        public List<string> EndpointWhitelist { get; set; }
        public List<string> ClientWhitelist { get; set; }
        public List<GeneralRules> GeneralRules { get; set; }
        public QuotaExceededResponse QuotaExceededResponse { get; set; }
    }

    /// <summary>
    /// Represents IpRateLimitPolicies configuration parameters
    /// </summary>
    public partial class IpRateLimitPolicies
    {
        public List<IpRules> IpRules { get; set; }
    }
}