﻿namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents FeatureManagement configuration parameters
    /// </summary>
    public partial class FeatureManagement
    {
        public bool Authentication { get; set; }
        public bool UseAutofac { get; set; }
        public bool HealthChecks { get; set; }
        public bool EnableDatabase { get; set; }
        public bool EnableRedisCache { get; set; }
        public bool EnableMemoryCache { get; set; }
        public bool EnableSwagger { get; set; }
        public bool EnableRedisRateLimiting { get; set; }
        public bool EnableSeq { get; set; }
    }
}