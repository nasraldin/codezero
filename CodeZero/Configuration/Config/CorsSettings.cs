﻿using CodeZero.Configuration.Models;
using System.Collections.Generic;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents CorsSettings configuration parameters
    /// </summary>
    public partial class CorsSettings
    {
        public bool EnableCors { get; set; } = false;
        public string DefaultCorsPolicy { get; set; }
        public List<CustomCorsPolicy> CorsPolicy { get; set; }
    }
}