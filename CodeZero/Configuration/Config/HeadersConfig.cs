﻿namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents Headers configuration parameters
    /// </summary>
    public partial class HeadersConfig
    {
        public string XFrameOptions { get; set; }
        public string XContentTypeOptions { get; set; }
        public string XssProtection { get; set; }
    }
}