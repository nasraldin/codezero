﻿using CodeZero.Configuration.Models;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents SwaggerInfo configuration parameters
    /// </summary>
    public partial class SwaggerInfo
    {
        public string Title { get; set; } = "CodeZero";
        public string Description { get; set; } = "CodeZero API Swagger";
        public Contact Contact { get; set; }
        public string TermsOfService { get; set; }
        public License License { get; set; }
    }
}