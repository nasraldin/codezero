﻿using CodeZero.Configuration.Models;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents Enforce Https configuration parameters
    /// </summary>
    public partial class ProxySettings
    {
        public bool EnableProxy { get; set; }
        public ForwardedHeadersOptions ForwardedHeadersOptions { get; set; }
        public HstsOptions HstsOptions { get; set; }
        public HttpsRedirectionOptions HttpsRedirectionOptions { get; set; }
        public string RequestBasePath { get; set; }
    }
}