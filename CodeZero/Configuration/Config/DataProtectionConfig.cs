﻿using CodeZero.Configuration.Models;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents DataProtection configuration parameters
    /// </summary>
    public partial class DataProtectionConfig
    {
        public bool EnableDataProtection { get; set; }
        public bool PersistKeysToRedis { get; set; }
        public string FileSystemDirectoryName { get; set; }
        public string RedisKey { get; set; }
        public KeyManagement KeyManagement { get; set; }
    }
}