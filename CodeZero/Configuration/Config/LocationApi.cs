﻿namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents Location configuration parameters
    /// </summary>
    public partial class LocationApi
    {
        /// <summary>
        /// Gets or sets the Url
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets AccessKey
        /// </summary>
        public string AccessKey { get; set; }
    }
}