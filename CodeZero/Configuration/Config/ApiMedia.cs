﻿using System.Collections.Generic;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents ApiMedia configuration parameters
    /// </summary>
    public partial class ApiMedia
    {
        public Dictionary<string, int> SupportedSizes { get; set; }
        public int MaxBrowserCacheDays { get; set; }
        public int MaxCacheDays { get; set; }
        public int MaxFileSize { get; set; }
        public string CdnBaseUrl { get; set; }
        public string AssetsRequestPath { get; set; }
        public string AssetsPath { get; set; }
        public bool UseTokenizedQueryString { get; set; }
        public Dictionary<string, string> AllowedFileExtensions { get; set; }
        public string ContentSecurityPolicy { get; set; }
    }
}