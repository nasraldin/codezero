﻿using CodeZero.Configuration.Models;

namespace CodeZero.Configuration
{
    /// <summary>
    /// Represents the app settings configuration
    /// </summary>
    public partial class ApiSettings
    {
        public string ApiName { get; set; }
        public string DisplayName { get; set; }
        public string DefaultCulture { get; set; }
        public DefaultApiVersion DefaultApiVersion { get; set; }
        public bool InitializeDatabase { get; set; }
        public bool EnableResponseCompression { get; set; }
        public bool EnableSessionAndCookies { get; set; }
        public bool AddContentTypeHeaders { get; set; }
        public bool AddContentLengthHeaders { get; set; }
        public bool IgnoreMissingFeatureFilters { get; set; }
        public bool RoutingLowercaseUrls { get; set; }
    }
}