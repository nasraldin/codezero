using CodeZero.Security.Permissions;

namespace CodeZero.Security
{
    public class StandardPermissions
    {
        public static readonly Permission SiteOwner = new Permission("SiteOwner", "Site Owners Permission", isSecurityCritical: true);
    }
}