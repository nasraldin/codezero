﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CodeZero.Security.AuthorizationHandlers
{
    /// <summary>
    /// This authorization handler validates any permission when the user is the service owner.
    /// </summary>
    public class SuperUserHandler : IAuthorizationHandler
    {
        public SuperUserHandler()
        {
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task HandleAsync(AuthorizationHandlerContext context)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            var userId = context?.User?.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null)
            {
                return;
            }

            if (string.Equals(userId, "admin", StringComparison.OrdinalIgnoreCase))
            {
                SucceedAllRequirements(context);
            }
        }

        private static void SucceedAllRequirements(AuthorizationHandlerContext context)
        {
            foreach (var requirement in context.Requirements.OfType<PermissionRequirement>())
            {
                context.Succeed(requirement);
            }
        }
    }
}