﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace CodeZero.Security
{
    public class CurrentUserClaims : ICurrentUserClaims
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        private class UserClaimsKey
        {
            public const string UserId = "sub";
            public const string UserName = "name";
            public const string UserEmail = "email";
        }

        public CurrentUserClaims(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public IEnumerable<Claim> GetCurrentUserClaims
        {
            get
            {
                return _httpContextAccessor.HttpContext?.User?.Claims;
            }
        }

        public string GetUserId()
        {
            return GetCurrentUserClaims?.FirstOrDefault(key => key.Type == UserClaimsKey.UserId)?.Value;
        }

        public string GetUserName()
        {
            return GetCurrentUserClaims?.FirstOrDefault(key => key.Type == UserClaimsKey.UserName)?.Value;
        }

        public string GetUserEmail()
        {
            return GetCurrentUserClaims?.FirstOrDefault(key => key.Type == UserClaimsKey.UserEmail)?.Value;
        }
    }
}