﻿using System.Collections.Generic;
using System.Security.Claims;

namespace CodeZero.Security
{
    public interface ICurrentUserClaims
    {
        IEnumerable<Claim> GetCurrentUserClaims { get; }
        string GetUserId();
        string GetUserName();
        string GetUserEmail();
    }
}