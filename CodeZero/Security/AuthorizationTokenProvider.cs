﻿//using IdentityModel.Client;
//using Microsoft.AspNetCore.Http;
//using System;
//using System.Net.Http;

//namespace CodeZero.Security
//{
//    public class AuthorizationTokenProvider : IAuthorizationTokenProvider
//    {
//        private readonly IHttpContextAccessor _httpContextAccessor;

//        public AuthorizationTokenProvider(IHttpContextAccessor httpContextAccessor)
//        {
//            _httpContextAccessor = httpContextAccessor;
//        }

//        private string _token = null;

//        public string Token
//        {
//            get
//            {
//                if (string.IsNullOrEmpty(_token) && _httpContextAccessor != null)
//                    _token = _httpContextAccessor.HttpContext?.Request?.Headers["Authorization"];
//                return _token;
//            }
//            internal set
//            {
//                _token = value;
//            }
//        }

//        public void InsureToken(string clientId, string clientSecret, string scopes)
//        {
//            var client = new HttpClient();
//            var disco = client.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
//            {
//                Address = "IdentityServiceUrl",
//                Policy = { RequireHttps = false, ValidateIssuerName = false }
//            }).Result;

//            if (disco.IsError)
//            {
//                throw new Exception($"Error communicating to Identity Server at: IdentityServiceUrl, {disco.Error}", disco.Exception);
//            }

//            var tokenResponse = client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
//            {
//                Scope = scopes,
//                ClientId = clientId,
//                ClientSecret = clientSecret,
//                Address = disco.TokenEndpoint
//            }).Result;

//            if (tokenResponse.IsError)
//            {
//                throw new Exception($"Error retrieving token for {clientId} with result: {tokenResponse.Error},{tokenResponse.ErrorDescription},{tokenResponse.Raw}", tokenResponse.Exception);
//            }

//            this.Token = "Bearer " + tokenResponse.AccessToken;
//        }
//    }
//}
