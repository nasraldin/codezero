﻿namespace CodeZero.Security
{
    public interface IAuthorizationTokenProvider
    {
        string Token { get; }
        void InsureToken(string clientId, string clientSecret, string scopes);
    }
}