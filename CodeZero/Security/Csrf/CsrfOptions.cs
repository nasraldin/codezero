﻿using System.Collections.Generic;

namespace CodeZero.Security.Csrf
{
    public class CsrfOptions
    {
        public string HeaderName { get; set; } = AppConsts.HeaderName.Csrf;
        public List<string> CsrfIgnoredUrls { get; set; }
    }
}