using Microsoft.Extensions.Primitives;
using System.Threading.Tasks;

namespace CodeZero.Caching
{
    public interface ISignal
    {
        IChangeToken GetToken(string key);
        Task SignalTokenAsync(string key);
    }
}