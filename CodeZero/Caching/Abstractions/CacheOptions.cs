namespace CodeZero.Caching
{
    public class CacheOptions
    {
        public bool Enabled { get; set; } = true;
        public bool DebugMode { get; set; }
    }
}