using CodeZero.Caching;
using CodeZero.Caching.CacheContextProviders;
using CodeZero.Caching.Distributed;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;

namespace Microsoft.Extensions.DependencyInjection
{
    public static partial class CodeZeroBuilderExtensions
    {
        /// <summary>
        /// Adds service level caching services.
        /// </summary>
        public static CodeZeroBuilder AddCaching(this CodeZeroBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.AddSingleton<ISignal>(sp =>
                {
                    var messageBus = sp.GetService<IMessageBus>();

                    if (messageBus == null)
                    {
                        return new Signal();
                    }
                    else
                    {
                        return new DistributedSignal(messageBus);
                    }
                });

                services.AddScoped<ITagCache, DefaultTagCache>();
                services.AddScoped<ICacheContextManager, CacheContextManager>();
                services.AddScoped<ICacheScopeManager, CacheScopeManager>();

                services.AddScoped<ICacheContextProvider, QueryCacheContextProvider>();
                services.AddScoped<ICacheContextProvider, RolesCacheContextProvider>();
                services.AddScoped<ICacheContextProvider, RouteCacheContextProvider>();
                services.AddScoped<ICacheContextProvider, UserCacheContextProvider>();
                services.AddScoped<ICacheContextProvider, KnownValueCacheContextProvider>();

                // IMemoryCache is registered at the service level so that there is one instance for each service.
                services.AddSingleton<IMemoryCache, MemoryCache>();

                // MemoryDistributedCache needs to be registered as a singleton as it owns a MemoryCache instance.
                services.AddSingleton<IDistributedCache, MemoryDistributedCache>();
            });

            return builder;
        }
    }
}