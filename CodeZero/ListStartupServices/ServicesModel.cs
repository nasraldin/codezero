using System.Collections.Generic;

namespace CodeZero.ListStartupServices
{
    public class ServicesModel
    {
        public List<Services> Services { get; set; }
        public int Count { get; set; }
    }

    public class Services
    {
        public ServiceProps ServiceType { get; set; }
        public string Lifetime { get; set; }
        public ServiceProps ImplementationType { get; set; }
    }

    public class ServiceProps
    {
        public string Name { get; set; }
        public string FullName { get; set; }
    }
}