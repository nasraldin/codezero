﻿using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace CodeZero.ListStartupServices
{
    public class ServiceConfig
    {
        public string Path { get; set; } = "/listallservices";
        public List<ServiceDescriptor> Services { get; set; } = new List<ServiceDescriptor>();
    }
}