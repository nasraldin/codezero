﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeZero.ListStartupServices
{
    public class ShowAllServicesMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ServiceConfig _config;
        private readonly ILogger<ShowAllServicesMiddleware> _logger;

        public ShowAllServicesMiddleware(RequestDelegate next, IOptions<ServiceConfig> config, ILogger<ShowAllServicesMiddleware> logger)
        {
            _next = next;
            _config = config.Value;
            _logger = logger;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.Path == _config.Path)
            {
                List<Services> services = new() { };

                foreach (var svc in _config.Services)
                {
                    services.AddIfNotContains(new Services
                    {
                        ServiceType = new ServiceProps
                        {
                            Name = svc.ServiceType.Name,
                            FullName = svc.ServiceType.FullName

                        },
                        Lifetime = svc.Lifetime.ToString(),
                        ImplementationType = new ServiceProps
                        {
                            Name = svc.ImplementationType?.Name,
                            FullName = svc.ImplementationType?.FullName

                        },
                    });
                }

                await httpContext.Response.WriteAsJsonAsync(new ServicesModel()
                {
                    Count = _config.Services.Count,
                    Services = services
                });
            }

            try
            {
                if (!httpContext.Response.HasStarted)
                    await _next.Invoke(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError("ShowAllServicesMiddleware: {Message}", ex.Message);
            }
        }
    }

    // Extension method used to add ShowAllServices middleware to the HTTP request pipeline.
    public static class ShowAllServicesMiddlewareExtensions
    {
        public static IApplicationBuilder UseShowAllServicesMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ShowAllServicesMiddleware>();
        }
    }
}