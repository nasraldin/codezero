namespace System.Transactions
{
    public static class IsolationLevelExtensions
    {
        /// <summary>
        /// Converts <see cref="IsolationLevel"/> to <see cref="IsolationLevel"/>.
        /// </summary>
        public static IsolationLevel ToSystemDataIsolationLevel(this IsolationLevel isolationLevel)
        {
            return isolationLevel switch
            {
                IsolationLevel.Chaos => IsolationLevel.Chaos,
                IsolationLevel.ReadCommitted => IsolationLevel.ReadCommitted,
                IsolationLevel.ReadUncommitted => IsolationLevel.ReadUncommitted,
                IsolationLevel.RepeatableRead => IsolationLevel.RepeatableRead,
                IsolationLevel.Serializable => IsolationLevel.Serializable,
                IsolationLevel.Snapshot => IsolationLevel.Snapshot,
                IsolationLevel.Unspecified => IsolationLevel.Unspecified,
                _ => throw new Exception("Unknown isolation level: " + isolationLevel),
            };
        }
    }
}