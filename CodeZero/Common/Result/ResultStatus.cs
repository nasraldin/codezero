﻿namespace CodeZero.Result
{
    public enum ResultStatus
    {
        Ok,
        Error,
        Forbidden,
        Invalid,
        NotFound
    }
}
