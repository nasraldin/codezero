﻿using CodeZero.Common.Models;
using Refit;
using System.Threading.Tasks;

namespace CodeZero.Common
{
    public interface IServiceLookup
    {
        [Get("")]
        Task<LookupOutput> GetAll(LookupFilter filter);
    }
}