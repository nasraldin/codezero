﻿using System.Threading.Tasks;

namespace CodeZero.Common
{
    public interface IAuthServerConnect
    {
        Task<string> RequestClientCredentialsTokenAsync();
    }
}
