﻿using CodeZero.Configuration;
using CodeZero.Security;
using CodeZero.Security.Csrf;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Refit;
using System;
using System.Net;
using System.Net.Http;

namespace CodeZero.Common.Refit
{
    public class RefitServiceResolver : IRefitServiceResolver
    {
        private readonly IAuthorizationTokenProvider _authorizationTokenProvider;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly SecurityConfigurations _securityConfigurations;

        public RefitServiceResolver(IAuthorizationTokenProvider authorizationTokenProvider
            , IHttpContextAccessor httpContextAccessor
            , IOptions<SecurityConfigurations> securityConfigurations)
        {
            _authorizationTokenProvider = authorizationTokenProvider;
            _httpContextAccessor = httpContextAccessor;
            _securityConfigurations = securityConfigurations.Value;
        }


        public T GetRefitService<T>(string serviceUrl)
        {
            //var refitsettings = new RefitSettings
            //{
            //    JsonSerializerSettings = CustomJsonSerializerSettings.Instance,
            //};

            var refitsettings = new RefitSettings();

            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(serviceUrl)
            };

            if (!string.IsNullOrWhiteSpace(_authorizationTokenProvider?.Token))
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", _authorizationTokenProvider.Token);
            }

            // Add API Name as http agent header for internal communication tracking
            httpClient.DefaultRequestHeaders.UserAgent.ParseAdd(GetUserAgent());

            // No need for CSRF with (cookieless) Rest API!
            //InjectCSRFSecuitry(httpClient);

            return RestService.For<T>(httpClient, refitsettings);
        }

        private string GetUserAgent()
        {
            var apiName = CoreConfiguration.Instance.ApiSettings.ApiName;
            var osName = Environment.OSVersion?.VersionString;
            var machineName = Environment.MachineName;

            return $"{apiName} ({osName}) {machineName}";
        }

        private void InjectCSRFSecuitry(HttpClient httpClient)
        {
            var isCsrfEnabled = Environment.GetEnvironmentVariable("IsCsrfEnabled");
            if (bool.Parse(isCsrfEnabled) == false)
            {
                return;
            }

            var headerName = !string.IsNullOrEmpty(_securityConfigurations?.CsrfOptions?.HeaderName) ? _securityConfigurations.CsrfOptions.HeaderName : AppConsts.HeaderName.Csrf;
            var csrfToken = _httpContextAccessor.HttpContext?.Request?.Headers[headerName];

            var orgRequestCookies = _httpContextAccessor.HttpContext?.Request.Cookies;
            //var baseAddress = !string.IsNullOrEmpty(_serviceSettings.BaseUrl) ? new Uri(_serviceSettings.BaseUrl) : new Uri("http://localhost");
            var baseAddress = new Uri("http://localhost");
            var cookieContainer = new CookieContainer();

            if (orgRequestCookies != null)
            {
                foreach (var cookie in orgRequestCookies)
                {
                    var ncookie = new Cookie()
                    {
                        Name = cookie.Key,
                        Value = cookie.Value,
                        //Domain = !string.IsNullOrEmpty(_serviceSettings?.BaseUrl)
                        Domain = baseAddress.Host,
                        HttpOnly = false,
                        Secure = false,
                        Path = "/"
                    };
                    httpClient.DefaultRequestHeaders.Add("Set-Cookie", ncookie.ToString());
                }
            }
            httpClient.DefaultRequestHeaders.Add(headerName, (string)csrfToken);
        }
    }
}