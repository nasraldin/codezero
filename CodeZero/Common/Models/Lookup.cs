﻿namespace CodeZero.Common.Models
{
    public class Lookup
    {
        public string Label { get; set; }
        public object Value { get; set; }
        public object Parent { get; set; }
    }
}