﻿using System.Collections.Generic;

namespace CodeZero.Common.Models
{
    public class ApiErrorListResult
    {
        public List<ApiErrorResult> Errors { get; set; }
    }
}