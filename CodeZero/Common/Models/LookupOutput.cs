﻿using System.Collections.Generic;

namespace CodeZero.Common.Models
{
    public class LookupOutput
    {
        public int TotalCount { get; set; }
        public int CurrentPage { get; set; }

        public List<Lookup> Values { get; set; }
    }
}