﻿namespace CodeZero.Common.Models
{
    public class ApiErrorResult
    {
        public ApiError Error { get; set; }
    }
}