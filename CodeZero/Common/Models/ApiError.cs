﻿using CodeZero.ExceptionHandling;

namespace CodeZero.Common.Models
{
    public class ApiError : IHasErrorCode, IHasErrorDetails
    {
        public string Code { get; set; }
        public string Details { get; set; }
        public string Message { get; set; }
    }
}