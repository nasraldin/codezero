﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CodeZero.Common.Models
{
    public class LookupFilter
    {
        [DataMember(Name = "label")]
        public string Label { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "skip")]
        public string Skip { get; set; }

        [DataMember(Name = "pageNumber")]
        public string PageNumber { get; set; }

        [DataMember(Name = "limit")]
        public string Limit { get; set; }

        [DataMember(Name = "valueToLookup")]
        public string ValueToLookup { get; set; }

        [DataMember(Name = "criteria")]
        public Dictionary<string, string> Criteria { get; set; }
    }
}