﻿namespace CodeZero.Common.Models
{
    public class ApiResultOutput
    {
        public bool Success { get; set; }
    }
}