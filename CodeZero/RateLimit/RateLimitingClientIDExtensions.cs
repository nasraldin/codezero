using AspNetCoreRateLimit;
using AspNetCoreRateLimit.Redis;
using CodeZero.Configuration;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// Registration of the dependency in a service container.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add IpRateLimiting based on client ID.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        /// <param name="configuration">The application configuration.</param>
        public static IServiceCollection AddRateLimitingClientID([NotNull] this IServiceCollection services, IConfiguration configuration)
        {
            // load general configuration from appsettings.json
            services.Configure<ClientRateLimitOptions>(configuration.GetSection("ClientRateLimiting"));

            // load ip rules from appsettings.json
            services.Configure<ClientRateLimitPolicies>(configuration.GetSection("ClientRateLimitPolicies"));

            // inject counter and rules stores
            services.AddInMemoryRateLimiting();

            if (CoreConfiguration.Instance.FeatureManagement.EnableRedisRateLimiting)
            {
                services.AddDistributedRateLimiting<AsyncKeyLockProcessingStrategy>();
                services.AddDistributedRateLimiting<RedisProcessingStrategy>();
                services.AddRedisRateLimiting();

                // Solution 1
                // inject counter and rules distributed cache stores
                services.AddSingleton<IIpPolicyStore, DistributedCacheIpPolicyStore>();
                services.AddSingleton<IRateLimitCounterStore, DistributedCacheRateLimitCounterStore>();
                services.AddSingleton<IRateLimitCounterStore, DistributedCacheRateLimitCounterStore>();

                // Solution 2 - in-memory cache fallback
                //services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
                //services.AddSingleton<IRateLimitCounterStore, RedisRateLimitCounterStore>();

                services.AddStackExchangeRedisCache(options =>
                {
                    options.ConfigurationOptions = new ConfigurationOptions
                    {
                        // silently retry in the background if the Redis connection is temporarily down
                        AbortOnConnectFail = false
                    };
                    options.Configuration = CoreConfiguration.Instance.RedisConfig.ConnectionString;
                    options.InstanceName = "AspNetRateLimit";
                });
            }

            if (!CoreConfiguration.Instance.FeatureManagement.EnableRedisRateLimiting)
            {
                services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
                services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            }

            // configuration (resolvers, counter key builders)
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();

            return services;
        }
    }
}


namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Configure HTTP request pipeline for the current path.
    /// </summary>
    public static partial class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Register the IpRateLimiting
        /// </summary>
        /// <param name="app">The <see cref="IApplicationBuilder"/>.</param>
        /// <returns><see cref="IApplicationBuilder"/></returns>
        public static IApplicationBuilder UseRateLimitingClientID([NotNull] this IApplicationBuilder app)
        {
            return app.UseClientRateLimiting();
        }
    }
}