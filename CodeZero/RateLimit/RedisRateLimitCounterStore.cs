﻿using AspNetCoreRateLimit;
using CodeZero.Configuration;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace CodeZero.RateLimit
{
    public class RedisRateLimitCounterStore : IRateLimitCounterStore
    {
        private readonly ILogger _logger;
        private readonly IRateLimitCounterStore _memoryCacheStore;
        private readonly RedisConfig _redisOptions;
        private readonly ConnectionMultiplexer _redis;

        public RedisRateLimitCounterStore(
            IOptions<RedisConfig> redisOptions,
            IMemoryCache memoryCache,
            ILogger<RedisRateLimitCounterStore> logger)
        {
            _logger = logger;
            _memoryCacheStore = new MemoryCacheRateLimitCounterStore(memoryCache);

            _redisOptions = redisOptions?.Value;
            _redis = ConnectionMultiplexer.Connect(_redisOptions.ConnectionString);
        }

        private IDatabase RedisDatabase => _redis.GetDatabase();

        public async Task<bool> ExistsAsync(string id, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return await TryRedisCommandAsync(
                () =>
                {
                    return RedisDatabase.KeyExistsAsync(id);
                },
                () =>
                {
                    return _memoryCacheStore.ExistsAsync(id, cancellationToken);
                });
        }

        public async Task<RateLimitCounter?> GetAsync(string id, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return await TryRedisCommandAsync(
                async () =>
                {
                    var value = await RedisDatabase.StringGetAsync(id);

                    if (!string.IsNullOrEmpty(value))
                    {
                        return JsonSerializer.Deserialize<RateLimitCounter?>(value);
                    }

                    return null;
                },
                () =>
                {
                    return _memoryCacheStore.GetAsync(id, cancellationToken);
                });
        }

        public async Task RemoveAsync(string id, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            _ = await TryRedisCommandAsync(
                async () =>
                {
                    await RedisDatabase.KeyDeleteAsync(id);

                    return true;
                },
                async () =>
                {
                    await _memoryCacheStore.RemoveAsync(id, cancellationToken);

                    return true;
                });
        }

        public async Task SetAsync(string id, RateLimitCounter? entry, TimeSpan? expirationTime = null, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            _ = await TryRedisCommandAsync(
                async () =>
                {
                    await RedisDatabase.StringSetAsync(id, JsonSerializer.Serialize(entry.Value), expirationTime);

                    return true;
                },
                async () =>
                {
                    await _memoryCacheStore.SetAsync(id, entry, expirationTime, cancellationToken);

                    return true;
                });
        }

        private async Task<T> TryRedisCommandAsync<T>(Func<Task<T>> command, Func<Task<T>> fallbackCommand)
        {
            if (CoreConfiguration.Instance.FeatureManagement.EnableRedisCache == true && _redis?.IsConnected == true)
            {
                try
                {
                    return await command();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Redis command failed: {ex}");
                }
            }

            return await fallbackCommand();
        }
    }
}