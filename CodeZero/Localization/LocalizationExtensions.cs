using CodeZero;
using CodeZero.Configuration;
using CodeZero.Localization;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add Localization
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddLocalizationServices([NotNull] this IServiceCollection services)
        {
            services.AddLocalization();
            // For performance, prevents the 'ResourceManagerStringLocalizer' from being used.
            // Also support pluralization.
            services.AddSingleton<IStringLocalizerFactory, NullStringLocalizerFactory>();
            services.AddSingleton<IHtmlLocalizerFactory, NullHtmlLocalizerFactory>();
            services.AddScoped<ILocalizationService, DefaultLocalizationService>();
            services.AddScoped<ICalendarManager, DefaultCalendarManager>();
            services.AddScoped<ICalendarSelector, DefaultCalendarSelector>();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                CultureInfo[] supportedCultures = CoreConfiguration.Instance.Languages.Select(lang => new CultureInfo(lang.Culture)).ToArray();

                options.DefaultRequestCulture = new RequestCulture(CoreConfiguration.Instance.ApiSettings.DefaultCulture);
                options.SupportedCultures = supportedCultures;
                options.RequestCultureProviders = new List<IRequestCultureProvider>
                {
                    // Order is important, its in which order they will be evaluated
                    new AcceptLanguageHeaderRequestCultureProvider(),
                    //new QueryStringRequestCultureProvider(),
                    //new CookieRequestCultureProvider()
                };
            });

            services.AddMvc().AddDataAnnotationsLocalization();

            return services;
        }
    }
}

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Configure HTTP request pipeline for the current path.
    /// </summary>
    public static partial class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Localization
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseLocalizationServices([NotNull] this IApplicationBuilder app)
        {
            CultureInfo[] supportedCultures = CoreConfiguration.Instance.Languages.Select(lang => new CultureInfo(lang.Culture)).ToArray();

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(CoreConfiguration.Instance.ApiSettings.DefaultCulture),
                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,
            });

            app.Use(async (context, next) =>
            {
                // Get client prefered language
                var userLangs = context.Request.Headers[AppConsts.HeaderName.AcceptLanguage].ToString();
                var firstLang = userLangs.Split(',').FirstOrDefault();

                // Set language
                var lang = supportedCultures.FirstOrDefault(lang => lang.Name == firstLang)?.Name ?? CoreConfiguration.Instance.ApiSettings.DefaultCulture;

                context.Response.Headers.TryAdd(AppConsts.HeaderName.ContentLanguage, lang);

                // Switch app culture
                Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);

                await next();
            });

            return app;
        }
    }
}