namespace CodeZero.Localization
{
    /// <summary>
    /// Represents an enum for calendar names
    /// </summary>
    public enum CalendarName
    {
        Hijri,
        UmAlQura,
        Gregorian,
        Persian,
        Julian,
        Hebrew,
        Unknown
    }
}