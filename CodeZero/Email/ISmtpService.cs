using Microsoft.Extensions.Logging;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CodeZero.Email
{
    /// <summary>
    /// Represents a contract for SMTP service.
    /// </summary>
    public interface ISmtpService
    {
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="message">The message to be sent.</param>
        /// <returns>A <see cref="SmtpResult"/> that holds information about the sent message, for instance if it has sent successfully or if it has failed.</returns>
        Task<SmtpResult> SendAsync(MailMessage message);
    }

    public interface IEmailSender
    {
        Task SendEmailAsync(string to, string from, string subject, string body);
    }

    //public class EmailSender : IEmailSender
    //{
    //    private readonly ILogger<EmailSender> _logger;

    //    public EmailSender(ILogger<EmailSender> logger)
    //    {
    //        _logger = logger;
    //    }

    //    public async Task SendEmailAsync(string to, string from, string subject, string body)
    //    {
    //        var emailClient = new SmtpClient("localhost");
    //        var message = new MailMessage
    //        {

    //            From = new MailAddress(from),
    //            Subject = subject,
    //            Body = body


    //        };
    //        message.To.Add(new MailAddress(to));
    //        await emailClient.SendMailAsync(message);
    //        _logger.LogWarning($"Sending email to {to} from {from} with subject {subject}.");
    //    }
    //}
}