using CodeZero.Email;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// </summary>
    public static partial class CodeZeroBuilderExtensions
    {
        /// <summary>
        /// Adds e-mail address validator service.
        /// </summary>
        /// <param name="builder">The <see cref="CodeZeroBuilder"/>.</param>
        public static CodeZeroBuilder AddEmailAddressValidator(this CodeZeroBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.AddTransient<IEmailAddressValidator, EmailAddressValidator>();
            });

            return builder;
        }
    }
}