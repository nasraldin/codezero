﻿namespace CodeZero.ApiVersioning
{
    public class NullRequestedApiVersion : IRequestedApiVersion
    {
        public static NullRequestedApiVersion Instance = new();

        public string Current => null;

        private NullRequestedApiVersion() { }
    }
}