﻿using System;

namespace CodeZero.ExceptionHandling
{
    public class RequestUnauthorizedException : CodeZeroException
    {
        public RequestUnauthorizedException(Exception inner) : base("Unauthorized", inner) { }
        public static void ThrowIfUnauthorizedServiceCall(Exception ex)
        {
            if (ex.InnerException != null && (ex.InnerException is Refit.ApiException exception) && exception.ReasonPhrase == "Unauthorized")
                throw new RequestUnauthorizedException(ex);
        }
    }
}