﻿namespace CodeZero.ExceptionHandling
{
    public class NotFoundException : CodeZeroException
    {
        public NotFoundException(string name, object key)
            : base($"Entity \"{name}\" ({key}) was not found.")
        {
        }
    }
}