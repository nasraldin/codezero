﻿namespace CodeZero.ExceptionHandling
{
    public static class ErrorCodes
    {
        public static readonly ErrorId NotModified = new ErrorId(304, "Not Modified");
        public static readonly ErrorId Redirect = new ErrorId(307, "Redirect");
        public static readonly ErrorId BadRequest = new ErrorId(400, "Bad Request");
        public static readonly ErrorId Unauthorized = new ErrorId(401, "Unauthorized");
        public static readonly ErrorId PaymentRequired = new ErrorId(402, "Payment Required");
        public static readonly ErrorId Forbidden = new ErrorId(403, "Forbidden");
        public static readonly ErrorId NotFound = new ErrorId(404, "Not Found");
        public static readonly ErrorId MethodNotAllowed = new ErrorId(405, "Method Not Allowed");
        public static readonly ErrorId RequestTimeout = new ErrorId(408, "Request Timeout");
        public static readonly ErrorId Conflict = new ErrorId(409, "Conflict");
        public static readonly ErrorId PayloadTooLarge = new ErrorId(413, "Payload Too Large");
        public static readonly ErrorId URITooLong = new ErrorId(414, "URI Too Long");
        public static readonly ErrorId ExpectationFailed = new ErrorId(417, "Expectation Failed");
        public static readonly ErrorId RequestExpired = new ErrorId(419, "Request Expired");
        public static readonly ErrorId Locked = new ErrorId(423, "Locked");
        public static readonly ErrorId TooManyRequests = new ErrorId(429, "Too Many Requests");
        public static readonly ErrorId NoResponse = new ErrorId(444, "No Response");
        public static readonly ErrorId Unavailable = new ErrorId(451, "Unavailable");
        public static readonly ErrorId ClientClosedRequest = new ErrorId(460, "Client Closed Request");
        public static readonly ErrorId InvalidToken = new ErrorId(498, "Invalid Token");
        public static readonly ErrorId TokenRequired = new ErrorId(499, "Token Required");
        public static readonly ErrorId InternalServerError = new ErrorId(500, "Internal Server Error");
        public static readonly ErrorId NotImplemented = new ErrorId(501, "Not Implemented");
        public static readonly ErrorId BadGateway = new ErrorId(502, "Bad Gateway");
        public static readonly ErrorId ServiceUnavailable = new ErrorId(503, "Service Unavailable");
        public static readonly ErrorId GatewayTimeout = new ErrorId(504, "Gateway Timeout");
        public static readonly ErrorId NotSupported = new ErrorId(505, "Not Supported");
        public static readonly ErrorId LoopDetected = new ErrorId(508, "Loop Detected");
        public static readonly ErrorId UnknownError = new ErrorId(520, "Unknown Error");
        public static readonly ErrorId ConnectionTimedOut = new ErrorId(522, "Connection Timed Out");
    }
}