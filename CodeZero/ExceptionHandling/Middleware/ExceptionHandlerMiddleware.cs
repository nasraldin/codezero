﻿using CodeZero.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace CodeZero.Middleware
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong: {ex}");
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            //var code = HttpStatusCode.InternalServerError;
            //var result = string.Empty;
            //switch (exception)
            //{
            //    case ValidationException validationException:
            //        code = HttpStatusCode.BadRequest;
            //        result = JsonConvert.SerializeObject(validationException.Failures);
            //        break;
            //    case NotFoundException _:
            //        code = HttpStatusCode.NotFound;
            //        break;
            //}

            //context.Response.ContentType = "application/json";
            //context.Response.StatusCode = (int)code;

            //if (string.IsNullOrEmpty(result))
            //{
            //        return context.Response.WriteAsync(new ErrorDetails()
            //        {
            //            StatusCode = context.Response.StatusCode,
            //            Message = "Internal Server Error from the custom middleware."
            //        }.ToString());

            //    result = JsonConvert.SerializeObject(new { error = exception.Message });
            //}
            //return context.Response.WriteAsync(result);

            // Hande exceptions conditionally by type:
            if (exception.GetType() == typeof(ArgumentException))
            {
                //Track the user that ran into the exception (for our structured logs)
                //var user = new User { Id = Guid.NewGuid(), Name = "Nasr Aldin" };
                var user = new { Id = Guid.NewGuid(), Name = AppSettings.Instance.ApplicationName };

                // Log our exception using Serilog.
                // Use structured logging to capture the full exception object.
                // Serilog provides the @ destructuring operator to help preserve object structure for our logs.
                _logger.LogError("Argument exception caught {@user} {@exception}", user, exception);

                var code = HttpStatusCode.OK; //<-- we do not respond with "InternalServerError" - but rather with isSuccess = false with exception message (this also avoids issues with OpenAPI/Swagger throwing an exception due to server response codes).
                var result = JsonConvert.SerializeObject(new
                {
                    isSuccess = false,
                    exceptionType = exception.GetType().ToString(),
                    message = exception.Message
                });

                context.Response.ContentType = AppConsts.MimeTypes.JSON;
                context.Response.StatusCode = (int)code;
                return context.Response.WriteAsync(result);
            }
            else
            {
                //Track the user that ran into the exception (for our structured logs)
                //var user = new User { Id = Guid.NewGuid(), Name = "Nasr Aldin" };
                var user = new { Id = Guid.NewGuid(), Name = AppSettings.Instance.ApplicationName };

                // Log our exception using Serilog.
                // Use structured logging to capture the full exception object.
                // Serilog provides the @ destructuring operator to help preserve object structure for our logs.
                _logger.LogError("Exception caught {@user} {@exception}", user, exception);

                //var code = HttpStatusCode.InternalServerError;
                var code = HttpStatusCode.OK; //<-- we do not respond with "InternalServerError" - but rather with isSuccess = false with exception message (this also avoids issues with OpenAPI/Swagger throwing an exception due to server response codes).
                //var result = JsonConvert.SerializeObject(exception, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore } );
                var result = JsonConvert.SerializeObject(new
                {
                    isSuccess = false,
                    exceptionType = exception.GetType().ToString(),
                    message = exception.Message
                });

                context.Response.ContentType = AppConsts.MimeTypes.JSON;
                context.Response.StatusCode = (int)code;
                return context.Response.WriteAsync(result);
            }
        }
    }
}