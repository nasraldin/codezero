﻿using CodeZero.Common.Models;
using CodeZero.ExceptionHandling;
using CodeZero.ExceptionHandling.Validation;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CodeZero.Middleware
{
    public class ServiceExceptionHandler
    {
        private readonly RequestDelegate _next;

        public ServiceExceptionHandler(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var response = context.Response;
            var code = HttpStatusCode.InternalServerError;
            var message = exception.Message;
            Guid errorId = Guid.NewGuid();
            var aggregateErrors = new List<ApiErrorResult>();

            Log.ForContext("Type", "Error")
               .ForContext("Exception", exception, destructureObjects: true)
               .ForContext("Request Url", context.Request.Path.Value, destructureObjects: true)
               .Error(exception, message + "-" + errorId, errorId);

            if (exception is ValidationException)
            {
                code = HttpStatusCode.BadRequest;
            }
            else if (exception is CodeZeroAggregateException)
            {
                //code = HttpStatusCode.BadRequest;
                //aggregateErrors = ResolveAggregateErrors(exception, code, errorId);
            }
            //else if (exception is ServiceForbiddenException)
            //{
            //    code = HttpStatusCode.Forbidden;
            //}
            else if (exception is NotFoundException)
            {
                code = HttpStatusCode.NotFound;
            }

            var error = new ApiError
            {
                Code = code.ToString(),
                Message = message
            };

            // Return the contract whether on list shape or single object based on the exception type.
            //
            //string result;
            if (exception is CodeZeroAggregateException)
            {
                //result = 
                //    JsonUtils<ApiErrorListResult>.Serialize(new ApiErrorListResult { Errors = aggregateErrors });
            }
            else
            {
                //ResolveRefitErrorIfAny(exception.InnerException, error);
                //result = JsonUtils<ApiErrorResult>.Serialize(error);
            }

            //response.StatusCode = error.Code;
            response.ContentType = "application/json";
            //await response.WriteAsync(result);
            await Task.CompletedTask;
        }

        private void ResolveRefitErrorIfAny(Exception exception, ApiErrorResult error)
        {
            try
            {
                if (exception is Refit.ValidationApiException)
                {
                    var refitError = (Refit.ValidationApiException)exception;
                    //error.Error.Code = refitError?.StatusCode ?? error.Error.Code;
                    error.Error.Message = refitError.Content?.Detail?.ToString() ?? error.Error.Message;
                }
                else if (exception is Refit.ApiException)
                {
                    var refitError = (Refit.ApiException)exception;
                    //error.Error.Code = (int?)refitError?.StatusCode ?? error.Error.Code;
                    //error.Error.Message = JsonUtils<ApiErrorResult>.Deserialize(refitError.Content)?.Error?.Message ?? error.Error.Message;
                }
            }
            catch (JsonException) { }
        }

        private List<ApiError> ResolveAggregateErrors(Exception exception, string code, Guid errorId)
        {
            var aggregateErrors = new List<ApiError>();

            foreach (var _ex in (exception as CodeZeroAggregateException).InnerExceptions)
            {
                aggregateErrors.Add(new ApiError()
                {
                    Code = code,
                    Message = _ex.Message
                });
            }
            return aggregateErrors;
        }
    }
}