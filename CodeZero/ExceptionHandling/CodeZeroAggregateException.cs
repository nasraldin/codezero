﻿using System;
using System.Collections.Generic;

namespace CodeZero.ExceptionHandling
{
    public class CodeZeroAggregateException : AggregateException
    {
        public CodeZeroAggregateException(IEnumerable<Exception> innerExceptions)
            : base(innerExceptions) { }
        public CodeZeroAggregateException(string message, IEnumerable<Exception> innerExceptions)
            : base(message, innerExceptions) { }
    }
}