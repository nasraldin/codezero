namespace CodeZero.Feature
{
    /// <summary>
    /// App Features Flags
    /// </summary>
    public static partial class FeatureFlags
    {
        public enum CoreFeature
        {
            /// <summary>
            /// Use authentication.
            /// </summary>
            Authentication,

            /// <summary>
            /// Use Autofac.
            /// </summary>
            UseAutofac,

            /// <summary>
            /// Enable Application DbContext.
            /// </summary>
            EnableDatabase,

            /// <summary>
            /// Enable Redis Cache.
            /// </summary>
            EnableRedisCache,

            /// <summary>
            /// Enable MemoryCache.
            /// </summary>
            EnableMemoryCache,

            /// <summary>
            /// Use Swagger.
            /// </summary>
            EnableSwagger,

            /// <summary>
            /// Enable Redis Cache for RateLimiting.
            /// </summary>
            EnableRedisRateLimiting,

            /// <summary>
            /// Enable EnableSeq.
            /// </summary>
            EnableSeq,

            /// <summary>
            /// Enable Health Checks.
            /// </summary>
            HealthChecks,

            /// <summary>
            /// Filter errors out.
            /// </summary>
            ErrorFilter,
        }
    }
}