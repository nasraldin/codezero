﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace CodeZero.FileScan.Providers
{
    class SemanticProvider : IScan
    {
        private string _connectionString = null;
        private HttpClient _client = null;

        public void Open(ScanConfiguration scannerConfiguration)
        {
            if (scannerConfiguration.ConnectionString.IsNullOrWhiteSpace() || scannerConfiguration.Username.IsNullOrWhiteSpace() ||
                scannerConfiguration.Password.IsNullOrWhiteSpace())
            {
                throw new CodeZeroException("Make sure you have opened the connection with a valid connection string, username and password [ErrorCode: S100]");
            }
            else
            {
                _client = new HttpClient();

                // Set the connection
                _connectionString = scannerConfiguration.ConnectionString;
                _client.DefaultRequestHeaders.Add("username", scannerConfiguration.Username);
                _client.DefaultRequestHeaders.Add("password", scannerConfiguration.Password);
            }
        }

        public async Task<string> Scan(byte[] bytes)
        {
            if (string.IsNullOrWhiteSpace(_connectionString) || _client == null)
            {
                throw new CodeZeroException("Make sure you have opened the connection with a valid connection string, username and password [ErrorCode: S101]");
            }
            else
            {
                Stream stream = await FileScanHelper.WriteBytesToStream(bytes);

                //reset the CURRENT postion of the stream
                if (stream.Position != 0)
                {
                    stream.Position = 0;
                }

                MultipartFormDataContent form = new MultipartFormDataContent
                {
                    //attaching the filestream to the request's multipart/form-data
                    { new StreamContent(stream), "file", "FileToScan" }
                };

                HttpResponseMessage response = null;

                try
                {
                    //sending the request
                    response = _client.PostAsync(_connectionString, form).Result;
                }
                catch (Exception ex)
                {
                    throw new CodeZeroException("Semantic scanning process failed, please check connection [ErrorCode: S200]", ex);
                }

                //getting the result
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        public async Task<string> Scan(Stream stream)
        {
            if (string.IsNullOrWhiteSpace(_connectionString) || _client == null)
            {
                throw new CodeZeroException("Make sure you have opened the connection with a valid connection string, username and password [ErrorCode: S102]");
            }
            else
            {
                //reset the CURRENT postion of the stream
                if (stream.Position != 0)
                {
                    stream.Position = 0;
                }

                MultipartFormDataContent form = new MultipartFormDataContent
                {
                    //attaching the filestream to the request's multipart/form-data
                    { new StreamContent(stream), "file", "FileToScan" }
                };

                HttpResponseMessage response = null;

                try
                {
                    //sending the request
                    response = await _client.PostAsync(_connectionString, form);
                }
                catch (Exception ex)
                {
                    throw new CodeZeroException("Semantic scanning process failed, please check connection [ErrorCode: S201]", ex);
                }

                //getting the result
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        public async Task<string> Scan(string url)
        {
            if (string.IsNullOrWhiteSpace(_connectionString) || _client == null)
            {
                throw new CodeZeroException("Make sure you have opened the connection with a valid connection string, username and password [ErrorCode: S103]");
            }
            else
            {
                Stream stream = await FileScanHelper.GetStreamFromHttpResponse(url);

                //reset the CURRENT postion of the stream
                if (stream.Position != 0)
                {
                    stream.Position = 0;
                }

                MultipartFormDataContent form = new MultipartFormDataContent
                {
                    //attaching the filestream to the request's multipart/form-data
                    { new StreamContent(stream), "file", "FileToScan" }
                };

                HttpResponseMessage response = null;

                try
                {
                    //sending the request
                    response = _client.PostAsync(_connectionString, form).Result;
                }
                catch (Exception ex)
                {
                    throw new CodeZeroException("Semantic scanning process failed, please check connection [ErrorCode: S202]", ex);
                }

                // Getting the result
                return response.Content.ReadAsStringAsync().Result;
            }
        }
    }
}