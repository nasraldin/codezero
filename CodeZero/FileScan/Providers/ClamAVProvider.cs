﻿using nClam;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CodeZero.FileScan.Providers
{

    class ClamAVProvider : IScan
    {
        private ClamClient _clam = null;

        public void Open(ScanConfiguration scannerConfiguration)
        {
            if (string.IsNullOrWhiteSpace(scannerConfiguration.ConnectionString))
            {
                throw new CodeZeroException("Make sure there is a valid connection string inside your appsettings file [ErrorCode: C100]");
            }
            else
            {
                if (_clam != null)
                {
                    if (!_clam.PingAsync().Result)
                    {
                        throw new CodeZeroException("ClamAV connection failed, please check connection settings [ErrorCode: C101]");
                    }
                }
                else
                {
                    var host = FileScanHelper.GetHost(scannerConfiguration.ConnectionString);
                    var port = FileScanHelper.GetPort(scannerConfiguration.ConnectionString);

                    _clam = new ClamClient(host, port);
                }
            }
        }

        public async Task<string> Scan(byte[] bytes)
        {
            if (!_clam.PingAsync().Result)
            {
                throw new CodeZeroException("ClamAV connection failed, please check connection [ErrorCode: C102]");
            }
            else
            {
                _ = "" + ScanResultStatus.UnCheck;
                string result;
                try
                {
                    var stream = await FileScanHelper.WriteBytesToStream(bytes);

                    if (stream.Position != 0)
                    {
                        stream.Position = 0;
                    }

                    // Scanning
                    result = CheckResult(await _clam.SendAndScanFileAsync(stream));
                }
                catch (Exception ex)
                {
                    throw new CodeZeroException("ClamAV scanning process failed, please check connection [ErrorCode: C200]", ex);
                }
                return result;
            }
        }

        public async Task<string> Scan(Stream stream)
        {
            if (!_clam.PingAsync().Result)
            {
                throw new CodeZeroException("ClamAV connection failed, please check connection settings [ErrorCode: C103]");
            }
            else
            {
                string result = "" + ScanResultStatus.UnCheck;
                try
                {
                    if (stream.Position != 0)
                    {
                        stream.Position = 0;
                    }

                    // Scanning
                    result = CheckResult(await _clam.SendAndScanFileAsync(stream));
                }
                catch (Exception ex)
                {
                    throw new CodeZeroException("ClamAV scanning process failed, please check connection [ErrorCode: C201]", ex);
                }
                return result;
            }
        }

        public async Task<string> Scan(string url)
        {
            if (!_clam.PingAsync().Result)
            {
                throw new CodeZeroException("ClamAV connection failed, please check connection settings [ErrorCode: C104]");
            }
            else
            {
                string result = "" + ScanResultStatus.UnCheck;
                try
                {
                    var stream = await FileScanHelper.GetStreamFromHttpResponse(url);

                    if (stream.Position != 0)
                    {
                        stream.Position = 0;
                    }

                    // Scanning
                    result = CheckResult(await _clam.SendAndScanFileAsync(stream));
                }
                catch (Exception ex)
                {
                    throw new CodeZeroException("ClamAV scanning process failed, please check connection [ErrorCode: C202]", ex);
                }
                return result;
            }
        }

        public string CheckResult(ClamScanResult scanResult)
        {
            return scanResult.Result switch
            {
                ClamScanResults.Clean => ScanResultStatus.Clean.ToString(),
                ClamScanResults.VirusDetected => throw new CodeZeroException(ScanResultStatus.VirusDetected + " - Details: -> " + scanResult.InfectedFiles.First().VirusName),
                ClamScanResults.Error => ScanResultStatus.Error + " - Details: -> " + scanResult.RawResult,
                _ => ScanResultStatus.Unknown.ToString(),
            };
        }
    }
}