﻿namespace CodeZero.FileScan
{
    public class ScanConfiguration
    {
        public string ConnectionString { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}