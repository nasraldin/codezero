﻿using System.IO;
using System.Threading.Tasks;

namespace CodeZero.FileScan
{
    public interface IScan
    {
        Task<string> Scan(byte[] bytes);
        Task<string> Scan(Stream stream);
        Task<string> Scan(string url);
        void Open(ScanConfiguration scannerConfiguration);
    }
}