﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace CodeZero.FileScan
{
    class FileScanHelper
    {
        public static async Task<Stream> WriteBytesToStream(byte[] input)
        {
            var fileStream = new FileStream("FileToScan", FileMode.Create);
            await fileStream.WriteAsync(input, 0, input.Length);

            return fileStream;
        }

        public static async Task<Stream> GetStreamFromHttpResponse(string url)
        {
            HttpClient client = new HttpClient();
            var stream = await WriteBytesToStream(await client.GetByteArrayAsync(url));

            return stream;
        }

        public static int GetPort(string url)
        {
            Uri uri = new Uri(url);
            var port = uri.Port;

            if (!(port < 65536 && port > 1023))
            {
                throw new Exception("Make sure there is a port number in the connection string and its between 1024 and 65535 [ErrorCode: P100]");
            }
            else
            {
                return port;
            }
        }

        public static string GetHost(string url)
        {
            Uri uri = new Uri(url);
            var host = uri.Host;

            if (string.IsNullOrWhiteSpace(host))
            {
                throw new Exception("Connection failed, please check connection settings [ErrorCode: H100]");
            }
            else
            {
                return host;
            }
        }
    }
}