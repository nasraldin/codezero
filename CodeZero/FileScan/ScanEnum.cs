﻿namespace CodeZero.FileScan
{
    public enum ScanType
    {
        /// <summary>
        /// Scan with ClamAV server
        /// </summary>
        ClamAV,

        /// <summary>
        /// Scan with Semantic
        /// </summary>
        Semantic
    }

    public enum ScanResultStatus
    {
        /// <summary>
        /// Indicates the value is not set.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Indicates the scan was successful and no viruses were found.
        /// </summary>
        Clean,

        /// <summary>
        /// Indicates the scan was successful and one or more viruses were found.
        /// </summary>
        VirusDetected,

        /// <summary>
        /// Indicates the scan was unsuccessful.
        /// </summary>
        Error,

        /// <summary>
        /// Indicates the scan was not check.
        /// </summary>
        UnCheck
    }
}