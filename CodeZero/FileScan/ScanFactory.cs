﻿using CodeZero.FileScan.Providers;

namespace CodeZero.FileScan
{
    public class ScanFactory
    {
        public static IScan CreateScanner(ScanType ScannerType)
        {
            return ScannerType switch
            {
                ScanType.ClamAV => new ClamAVProvider(),
                ScanType.Semantic => new SemanticProvider(),
                _ => new SemanticProvider(),
            };
        }
    }
}