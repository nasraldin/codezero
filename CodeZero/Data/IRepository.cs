using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using CodeZero.Data.Query;
using CodeZero.Domain.Entities;
using Microsoft.EntityFrameworkCore.Query;

namespace CodeZero.Data
{
    public interface IRepository<TEntity, in TKey>
        where TEntity : BaseEntity<TKey>
        where TKey : struct
    {
        TEntity Add(TEntity entity);
        Task<TEntity> AddAsync(TEntity entity, CancellationToken cancellationToken);

        void Delete(TKey id);
        void Delete(TEntity entity);
        Task DeleteAsync(TKey id, CancellationToken cancellationToken);
        Task DeleteAsync(TEntity entity, CancellationToken cancellationToken);

        bool Exists(TKey id);
        Task<bool> ExistsAsync(TKey id, CancellationToken cancellationToken);

        TEntity GetById(TKey id, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = default, bool asTracking = default);
        Task<TEntity> GetByIdAsync(TKey id, CancellationToken cancellationToken, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = default, bool asTracking = default);

        void Update(TEntity entity);
        Task UpdateAsync(TEntity entity, CancellationToken cancellationToken);

        PagedResult<TEntity> GetAll(
            PageQuery pageQuery,
            Expression<Func<TEntity, bool>> predicate = default,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = default,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = default,
            bool asTracking = default);
        
        PagedResult<TResult> GetAllProjections<TResult>(
            PageQuery pageQuery,
            Expression<Func<TEntity, TResult>> selector = default,
            Expression<Func<TEntity, bool>> predicate = default,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = default,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = default,
            bool asTracking = default) 
            where TResult : class;

        Task<PagedResult<TEntity>> GetAllAsync(
            PageQuery pageQuery,
            CancellationToken cancellationToken,
            Expression<Func<TEntity, bool>> predicate = default,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = default,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = default,
            bool asTracking = default);
        
        Task<PagedResult<TResult>> GetAllProjectionsAsync<TResult>(
            PageQuery pageQuery,
            CancellationToken cancellationToken,
            Expression<Func<TEntity, TResult>> selector = default,
            Expression<Func<TEntity, bool>> predicate = default,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = default,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = default,
            bool asTracking = default)
            where TResult : class;
    }
}