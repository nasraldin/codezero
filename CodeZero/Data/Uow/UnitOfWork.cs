using CodeZero.Data.Transactions;
using CodeZero.Data.Transactions.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CodeZero.Data.Uow
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseFacade _database;
        private readonly DbContext _dbContext;
        private readonly AppTransactionOptions _options;
        //private readonly INotificationContext _notificationContext; // TODO: add notification

        public UnitOfWork(DbContext dbContext,
            IOptionsSnapshot<AppTransactionOptions> optionsMonitor
            /*INotificationContext notificationContext*/)
        {
            _dbContext = dbContext;
            _options = optionsMonitor.Value;
            _database = _dbContext.Database;
            //_notificationContext = notificationContext;
        }

        public TResult BeginTransaction<TResult>(Func<TResult> operation, Func<bool> condition)
            => CreateExecutionStrategy().Execute(() => ExecuteInScope(operation, condition));

        public Task<TResult> BeginTransactionAsync<TResult>(Func<CancellationToken, Task<TResult>> operationAsync,
            Func<CancellationToken, Task<bool>> condition, CancellationToken cancellationToken)
            => CreateExecutionStrategy().ExecuteAsync(ct => ExecuteInScopeAsync(operationAsync, condition, ct), cancellationToken);

        private Task<TResult> ExecuteInScopeAsync<TResult>(Func<CancellationToken, Task<TResult>> operationAsync,
            Func<CancellationToken, Task<bool>> condition, CancellationToken cancellationToken)
            => operationAsync
                .BeginTransactionScope()
                .WithScopeOption()
                .WithOptions(options => options.IsolationLevel = _options.IsolationLevel)
                .WithScopeAsyncFlowOption()
                .WithConditionAsync(condition /*?? (_ => _notificationContext.AllValidAsync)*/) // TODO: add notification
                .ExecuteAsync(cancellationToken);

        private TResult ExecuteInScope<TResult>(Func<TResult> operation, Func<bool> condition)
            => operation
                .BeginTransactionScope()
                .WithScopeOption()
                .WithOptions(options => options.IsolationLevel = _options.IsolationLevel)
                .WithScopeAsyncFlowOption()
                .WithCondition(condition /*?? (() => _notificationContext.AllValid)*/) // TODO: add notification
                .Execute();

        private IExecutionStrategy CreateExecutionStrategy() => _database.CreateExecutionStrategy();

        public bool SaveChanges() => _dbContext.SaveChanges(false) > 0;

        public async Task<bool> SaveChangesAsync(CancellationToken cancellationToken)
            => await _dbContext.SaveChangesAsync(false, cancellationToken) > 0;

        public Task<bool> CommitAsync()
        {
            throw new NotImplementedException();
        }
    }
}