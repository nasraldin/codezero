﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CodeZero.Data.Uow
{
    public interface IUnitOfWork
    {
        //void BeginTransaction();
        TResult BeginTransaction<TResult>(Func<TResult> operation, Func<bool> condition);
        Task<TResult> BeginTransactionAsync<TResult>(Func<CancellationToken,
            Task<TResult>> operationAsync,
            Func<CancellationToken, Task<bool>> condition,
            CancellationToken cancellationToken = default);

        bool SaveChanges();
        Task<bool> SaveChangesAsync(CancellationToken cancellationToken = default);

        Task<bool> CommitAsync();
        //Task<bool> CompleteAsync(CancellationToken cancellationToken = default);
        //Task<bool> RollbackAsync(CancellationToken cancellationToken = default);
        //void OnCompleted(Func<Task> handler);
    }
}