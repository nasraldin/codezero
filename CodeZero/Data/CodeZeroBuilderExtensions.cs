using CodeZero;
using CodeZero.Data;
using CodeZero.Data.Transactions;
using CodeZero.Data.Uow;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Scrutor;
using System;
using System.Transactions;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add data provider services
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddDataProvider(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            //var env = services.BuildServiceProvider().GetService<IWebHostEnvironment>();
            //var Configuration = services.BuildServiceProvider().GetService<IConfiguration>();

            //var databaseSetting = new DatabaseSetting();
            //// Bind Database setting to databasesetting variable
            //Configuration.Bind("DatabaseSetting", databaseSetting);

            //// DI : Configure DatabaseSetting Options => The options pattern uses classes to provide strongly typed access to groups of related settings
            //services.Configure<DatabaseSetting>(options => Configuration?.GetSection("DatabaseSetting").Bind(options));

            //if (string.IsNullOrWhiteSpace(databaseSetting.Type) || string.IsNullOrWhiteSpace(databaseSetting.ConnectionString))
            //{
            //    throw new Exception("Verify Database Settings in appsetting.json or appsetting.{Environment}.json");
            //}

            //switch (databaseSetting.Type)
            //{
            //    case "sqlserver":
            //        services.RegisterSqlServerDbContexts(databaseSetting.ConnectionString);
            //        break;
            //    case "pgsql":
            //        services.RegisterPgSqlDbContexts(databaseSetting.ConnectionString);
            //        break;
            //    case "mysql":
            //        services.RegisterMySQLDbContexts(databaseSetting.ConnectionString);
            //        break;

            //    case "mariadb":
            //        services.RegisterMariaDbContexts(databaseSetting.ConnectionString);
            //        break;

            //    default:
            //        throw new ArgumentException($"XtraSpurt Does Not Support : {databaseSetting.Type}");
            //}

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
            => services
                .Scan(selector => selector.FromAssemblies(Application.Assemblies)
                    .AddClasses(filter => filter.AssignableTo(typeof(IRepository<,>)))
                    .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                    .AsImplementedInterfaces()
                    .WithScopedLifetime());

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
            => services.AddScoped<IUnitOfWork, UnitOfWork>();

        public static OptionsBuilder<AppTransactionOptions> ConfigureTransactionOptions(this IServiceCollection services, IConfigurationSection section)
            => services
                .AddOptions<AppTransactionOptions>()
                .Bind(section)
                .Validate(options => options.IsolationLevel is not IsolationLevel.Unspecified);

    }
}