﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CodeZero.Data.Query
{
    public class PagedResult<T> // where T : class
    {
        //public IEnumerable<T> Item { get; set; }
        //public int Count { get; set; }
        private readonly IReadOnlyCollection<T> _items;
        private readonly int _index;
        private readonly int _size;

        public PagedResult(IReadOnlyCollection<T> items, int index, int size)
        {
            _items = items;
            _index = index;
            _size = size;
        }

        public IReadOnlyCollection<T> Items => _items.Take(_size).ToList();

        public PageInfo PageInfo
            => new()
            {
                Current = _index,
                Size = Items.Count,
                HasNext = _size < _items.Count,
                HasPrevious = _index > 1
            };

        public static async Task<PagedResult<T>> CreateAsync(IQueryable<T> source, PageQuery pageQuery, CancellationToken cancellationToken)
        {
            pageQuery ??= new();
            var items = await ApplyPagination(source, pageQuery).ToListAsync(cancellationToken);
            return new(items, pageQuery.Index, pageQuery.Size);
        }

        public static PagedResult<T> Create(IQueryable<T> source, PageQuery pageQuery)
        {
            pageQuery ??= new();
            var items = ApplyPagination(source, pageQuery).ToList();
            return new(items, pageQuery.Index, pageQuery.Size);
        }

        private static IQueryable<T> ApplyPagination(IQueryable<T> source, PageQuery pageQuery)
            => source.Skip(pageQuery.Size * (pageQuery.Index - 1)).Take(pageQuery.Size + 1);
    }
}