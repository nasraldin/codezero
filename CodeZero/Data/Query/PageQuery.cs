﻿using CodeZero.Configuration;

namespace CodeZero.Data.Query
{
    /// <summary>
    /// Represents a Page Params
    /// </summary>
    public class PageQuery
    {
        private readonly int DefaultSize = CoreConfiguration.Instance.PaginationOptions.DefaultPageSize;
        private const int DefaultIndex = 1;

        private readonly int _index;
        private readonly int _size;

        public int Size
        {
            get => _size <= 0 ? DefaultSize : _size;
            init => _size = value;
        }

        /// <summary>
        ///  Page Number
        /// </summary>
        public int Index
        {
            get => _index <= 0 ? DefaultIndex : _index;
            init => _index = value;
        }

        public string SortDirection { get; set; }
        public string SortColumnName { get; set; }
    }
}