﻿namespace CodeZero.Data.Query
{
    /// <summary>
    /// Represents a Page Info
    /// </summary>
    public record PageInfo
    {
        public int Current { get; init; }
        public int Size { get; init; }
        public int Count { get; init; }
        public int TotalPages { get; init; }
        public bool HasPrevious { get; init; }
        public bool HasNext { get; init; }
    }
}