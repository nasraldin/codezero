﻿using CodeZero.Data.Query;
using CodeZero.Data.Repositories;
using System;
using System.Collections.Generic;

namespace CodeZero.Data.Lookup
{
    public interface IServiceLookupRepository<T> : IRepository
    {
        IEnumerable<T> Find(FormattableString whereClause, object parameters);
        PagedResult<T> Find(FormattableString whereClause, object parameters, PageQuery pageQuery);
    }
}