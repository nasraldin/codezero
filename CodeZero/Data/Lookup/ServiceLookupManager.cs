﻿using CodeZero.Common.Models;
using CodeZero.Data.Query;
using System;
using System.Collections.Generic;

namespace CodeZero.Data.Lookup
{
    public class ServiceLookupManager<T> : IServiceLookupManager<T>
    {
        private readonly IServiceLookupRepository<T> _repository;

        public ServiceLookupManager(IServiceLookupRepository<T> repository)
        {
            _repository = repository;
        }

        //public virtual IEnumerable<T> GetAll()
        //{
        //    return _repository.GetAll();
        //}

        public virtual PagedResult<T> GetFiltered(FormattableString whereClause, object parameters, LookupFilter filters)
        {
            long? skip;

            int.TryParse(filters.Limit, out int limit);
            int.TryParse(filters.PageNumber, out int pageNumber);

            if (limit <= 0)
                limit = 25;

            if (string.IsNullOrEmpty(filters.Skip))
                skip = null;
            else
                skip = long.Parse(filters.Skip);

            var sortBy = filters.Criteria != null && filters.Criteria.ContainsKey("sortBy") && !string.IsNullOrEmpty(filters.Criteria["sortBy"]) ?
                         filters.Criteria["sortBy"] :
                         "Id";

            var sortDir = filters.Criteria != null && filters.Criteria.ContainsKey("sortDir") && !string.IsNullOrEmpty(filters.Criteria["sortDir"]) ?
                         filters.Criteria["sortDir"] :
                         "ASC";

            var pagination = new PageQuery
            {
                //Skip = skip,
                Size = limit,
                Index = pageNumber,
                SortColumnName = sortBy,
                SortDirection = sortDir
            };

            return _repository.Find(whereClause, parameters, pagination);
        }

        public virtual IEnumerable<T> GetSpecific(FormattableString whereClause, object parameters)
        {
            return _repository.Find(whereClause, parameters);
        }
    }
}