﻿using CodeZero.Common.Models;

namespace CodeZero.Data.Lookup
{
    public class FilterationUtils
    {
        public static bool IsFilterProvided(string filterValue)
        {
            return !string.IsNullOrEmpty(filterValue);
        }

        public static bool IsAnyFilterProvided(params string[] filterValues)
        {
            foreach (var value in filterValues)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsFilterCriteriaProvided(LookupFilter filter, string criteriaKey)
        {
            return filter?.Criteria != null && filter.Criteria.ContainsKey(criteriaKey) && !string.IsNullOrEmpty(filter.Criteria[criteriaKey]);
        }

        public static bool AreAllFilterCriteriaProvided(LookupFilter filter, params string[] criteriaKey)
        {
            if (filter?.Criteria == null)
            {
                return false;
            }

            foreach (var key in criteriaKey)
            {
                if (!filter.Criteria.ContainsKey(key) || string.IsNullOrEmpty(filter.Criteria[key]))
                {
                    return false;
                }
            }

            return true;
        }
    }
}