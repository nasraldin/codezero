﻿using CodeZero.Common.Models;
using CodeZero.Data.Query;
using System;
using System.Collections.Generic;

namespace CodeZero.Data.Lookup
{
    public interface IServiceLookupManager<T>
    {
        //IEnumerable<T> GetAll();

        PagedResult<T> GetFiltered(FormattableString whereClause, object parameters, LookupFilter filters);
        IEnumerable<T> GetSpecific(FormattableString whereClause, object parameters);
    }
}