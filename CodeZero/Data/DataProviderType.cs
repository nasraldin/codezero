﻿using System.Runtime.Serialization;

namespace CodeZero.Data
{
    /// <summary>
    /// Represents data provider type enumeration
    /// </summary>
    public enum DataProviderType
    {
        /// <summary>
        /// MS SQL Server
        /// </summary>
        [EnumMember(Value = "sqlserver")]
        SqlServer,

        /// <summary>
        /// MySQL
        /// </summary>
        [EnumMember(Value = "mysql")]
        MySql,

        /// <summary>
        /// MariaDB
        /// </summary>
        [EnumMember(Value = "mariadb")]
        MariaDB,

        /// <summary>
        /// PostgreSQL
        /// </summary>
        [EnumMember(Value = "postgresql")]
        PostgreSQL,

        /// <summary>
        /// MongoDB
        /// </summary>
        [EnumMember(Value = "mongodb")]
        MongoDB,

        /// <summary>
        /// CouchDB
        /// </summary>
        [EnumMember(Value = "couchdb")]
        CouchDB,

        /// <summary>
        /// CouchDB
        /// </summary>
        [EnumMember(Value = "couchbase")]
        CouchBase,

        /// <summary>
        /// Cassandra
        /// </summary>
        [EnumMember(Value = "cassandra")]
        Cassandra,

        /// <summary>
        /// Redis
        /// </summary>
        [EnumMember(Value = "redis")]
        Redis,

        /// <summary>
        /// Unknown
        /// </summary>
        [EnumMember(Value = "")]
        Unknown
    }
}