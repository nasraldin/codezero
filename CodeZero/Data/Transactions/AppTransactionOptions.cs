﻿using System.Transactions;

namespace CodeZero.Data.Transactions
{
    public class AppTransactionOptions
    {
        public IsolationLevel IsolationLevel { get; set; }
    }
}