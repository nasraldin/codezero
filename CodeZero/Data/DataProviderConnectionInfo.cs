﻿using CodeZero.Configuration;
using Microsoft.Extensions.Configuration;
using System;

namespace CodeZero.Data
{
    public class DataProviderConnectionInfo
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string Database { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public DataProviderConnectionInfo() { }
        public DataProviderConnectionInfo(string host, string port, string database, string userName, string password)
        {
            Host = AppSettings.Instance.Configuration.GetValue<string>(host);
            Port = AppSettings.Instance.Configuration.GetValue<string>(port);
            Database = AppSettings.Instance.Configuration.GetValue<string>(database);
            UserName = AppSettings.Instance.Configuration.GetValue<string>(userName);
            Password = AppSettings.Instance.Configuration.GetValue<string>(password);

            CheckConnectionInfo();
        }

        public void CheckConnectionInfo()
        {
            if (Host.IsNullOrEmpty() || UserName.IsNullOrEmpty() || Password.IsNullOrEmpty())
                throw new ArgumentNullException(nameof(DataProviderConnectionInfo));
        }
    }
}