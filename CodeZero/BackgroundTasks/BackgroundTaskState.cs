using System;

namespace CodeZero.BackgroundTasks
{
    public class BackgroundTaskState
    {
        public string Name { get; set; }
        public DateTime LastStartTime { get; set; }
    }
}