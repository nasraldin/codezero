﻿using CodeZero.ExceptionHandling;
using CodeZero.Security;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CodeZero.HostedService
{
    public abstract class BaseIdentityHostedService : BaseHostedService
    {
        protected BaseIdentityHostedService(IServiceProvider serviceProvider, ILogger logger)
            : base(serviceProvider, logger)
        {
        }

        protected abstract string HostedServiceName { get; }
        protected abstract string HostedServiceClientSecret { get; }
        protected abstract string HostedServiceRequestedScopes { get; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Delay(1);//return the execution point to the caller
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var tokenProvider = scope.ServiceProvider.GetService<IAuthorizationTokenProvider>();
                        tokenProvider.InsureToken(HostedServiceName, HostedServiceClientSecret, HostedServiceRequestedScopes);

                        while (!stoppingToken.IsCancellationRequested)
                        {
                            _logger.Debug($"Starting Job {GetType().FullName} - { DateTime.Now.ToString() }");
                            try
                            {
                                ExecuteJob(scope, stoppingToken);
                                _logger.Debug($"Completing Job {GetType().FullName} - { DateTime.Now.ToString() }");
                            }
                            catch (Exception ex)
                            {
                                RequestUnauthorizedException.ThrowIfUnauthorizedServiceCall(ex);
                                _logger.Error(ex, $"Failure in executing job {GetType().FullName} - { DateTime.Now.ToString() }");
                            }
                            await Task.Delay(HostedServiceInterval, stoppingToken);
                        }
                    }
                }
                catch (TaskCanceledException) { }
                catch (RequestUnauthorizedException)
                {
                    _logger.Debug($"Restarting job due to unauthorized attempt to call external service {GetType().FullName} - {DateTime.Now}");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, $"Failure in pre-execution of job {GetType().FullName} - {DateTime.Now}");
                }
                await Task.Delay(HostedServiceInterval, stoppingToken);
            }
        }
    }
}