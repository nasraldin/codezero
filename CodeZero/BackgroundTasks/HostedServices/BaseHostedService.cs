﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CodeZero.HostedService
{
    public abstract class BaseHostedService : IHostedService, IDisposable
    {
        private Task _executingTask;
        private readonly CancellationTokenSource _stoppingCts = new CancellationTokenSource();
        protected readonly ILogger _logger;
        protected readonly IServiceProvider _serviceProvider;

        protected BaseHostedService(IServiceProvider serviceProvider, ILogger logger)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        protected virtual int HostedServiceInterval => 5000;

        protected virtual async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Delay(1);//return the execution point to the caller
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.Debug($"Starting Job {GetType().FullName} - {DateTime.Now}");
                try
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        ExecuteJob(scope, stoppingToken);
                    }
                    _logger.Debug($"Completing Job {GetType().FullName} - {DateTime.Now}");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, $"Failure in executing job {GetType().FullName} - {DateTime.Now}");
                }
                await Task.Delay(HostedServiceInterval, stoppingToken);
            }
        }

        protected abstract void ExecuteJob(IServiceScope scope, CancellationToken stoppingToken);

        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            // Store the task we're executing
            _executingTask = ExecuteAsync(_stoppingCts.Token);

            // If the task is completed then return it, 
            // this will bubble cancellation and failure to the caller
            if (_executingTask.IsCompleted)
            {
                _logger.Debug($"The task '{GetType().FullName}' is completed unexpectedly!");
                return _executingTask;
            }

            // Otherwise it's running
            return Task.CompletedTask;
        }

        public virtual async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.Debug($"Receiving a stop request for job {GetType().FullName}");
            // Stop called without start
            if (_executingTask == null)
            {
                return;
            }

            try
            {
                // Signal cancellation to the executing method
                _logger.Debug($"Cancellation signalled to for job {GetType().FullName} - {DateTime.Now}");
                _stoppingCts.Cancel();
            }
            finally
            {
                // Wait until the task completes or the stop token triggers
                await Task.WhenAny(_executingTask, Task.Delay(Timeout.Infinite, cancellationToken));
                _logger.Debug($"{(_executingTask.IsCompleted ? "Successful" : "Usuccessful")} cancellation for job {GetType().FullName} - {DateTime.Now}");
            }

        }

        public virtual void Dispose()
        {
            _stoppingCts.Cancel();
        }
    }
}