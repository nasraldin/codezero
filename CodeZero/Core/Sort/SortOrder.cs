﻿namespace CodeZero.Sort
{
    public enum SortOrder
    {
        Ascending,
        Descending
    };
}