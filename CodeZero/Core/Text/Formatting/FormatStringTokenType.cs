namespace CodeZero.Text.Formatting
{
    internal enum FormatStringTokenType
    {
        ConstantText,
        DynamicValue
    }
}