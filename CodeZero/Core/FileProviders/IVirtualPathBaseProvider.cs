using Microsoft.AspNetCore.Http;

namespace CodeZero.FileProviders
{
    public interface IVirtualPathBaseProvider
    {
        PathString VirtualPathBase { get; }
    }
}
