﻿using System.Threading;

namespace CodeZero.Threading
{
    public interface ICancellationTokenProvider
    {
        CancellationToken Token { get; }
    }
}