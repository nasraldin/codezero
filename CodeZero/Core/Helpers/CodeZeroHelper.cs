using CodeZero.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Runtime.InteropServices;

namespace CodeZero
{
    public class CodeZeroHelper
    {
        /// <summary>
        /// Provides access to the current <see cref="IHttpContextAccessor.HttpContext"/>, if one is available.
        /// </summary>
        public static HttpContext HttpContext = HttpContextHelper.Current.HttpContextAccessor.HttpContext;

        /// <summary>
        /// Get the client IP address.
        /// </summary>
        /// <returns>ClientIpAddress</returns>
        public static string GetClientIPAddress()
        {
            return HttpContextHelper.Current.HttpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        }

        /// <summary>
        /// Get the client User-Agent.
        /// </summary>
        /// <returns>ClientIpAddress</returns>
        public static string GetUserAgent()
        {
            return HttpContextHelper.Current.HttpContextAccessor.HttpContext.Request.Headers[AppConsts.HeaderName.UserAgent].ToString();
        }

        /// <summary>
        /// Get the client OS Platform.
        /// </summary>
        /// <returns>OSPlatform</returns>
        public static string GetUserPlatform()
        {
            var userAgent = HttpContextHelper.Current.HttpContextAccessor.HttpContext.Request.Headers[AppConsts.HeaderName.UserAgent].ToString();
            foreach (AppConsts.OSPlatform os in (AppConsts.OSPlatform[])Enum.GetValues(typeof(AppConsts.OSPlatform)))
            {
                if (userAgent.Contains(os.GetStringValue()))
                    return os.GetDescription();
            }

            return AppConsts.OSPlatform.Unknown.GetDescription();
        }

        /// <summary>
        /// Get the Current OSPlatform.
        /// </summary>
        /// <returns>OSPlatform</returns>
        public static string GetCurrentOSPlatform()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return OSPlatform.Windows.ToString();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                return OSPlatform.OSX.ToString();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                return OSPlatform.Linux.ToString();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.FreeBSD))
                return OSPlatform.FreeBSD.ToString();

            return AppConsts.OSPlatform.Unknown.GetDescription();
        }

        /// <summary>
        /// Get the client Browser.
        /// </summary>
        /// <returns>Browser</returns>
        public static string GetUserBrowser()
        {
            var userAgent = HttpContextHelper.Current.HttpContextAccessor.HttpContext.Request.Headers[AppConsts.HeaderName.UserAgent].ToString();
            foreach (AppConsts.BrowserPlatform br in (AppConsts.BrowserPlatform[])Enum.GetValues(typeof(AppConsts.BrowserPlatform)))
            {
                if (userAgent.Contains(br.GetStringValue()))
                    return br.GetDescription();
            }

            return AppConsts.OSPlatform.Unknown.GetDescription();
        }
    }
}