﻿using Microsoft.AspNetCore.Http;
using System.Text.Json;

namespace CodeZero.Helpers
{
    /// <summary>
    /// Provides access to the current <see cref="ISession"/>, if one is available.
    /// </summary>
    public static class HttpSessionHelper
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonSerializer.Serialize(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default : JsonSerializer.Deserialize<T>(value);
        }
    }
}