using System;
using System.Collections.Generic;

namespace CodeZero
{
    public static class MediaHelper
    {
        private static readonly HashSet<string> _imageExtensions = new(StringComparer.OrdinalIgnoreCase)
        { ".bmp", ".gif", ".jpeg", ".jpg", ".png", ".tiff", ".webp" };

        /// <summary>
        /// Determines if a path is an image file.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>true if the path is an image file. Otherwise false.</returns>
        public static bool IsImageFile(string path)
        {
            return _imageExtensions.Contains(System.IO.Path.GetExtension(path));
        }
    }
}