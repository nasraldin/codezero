﻿using CodeZero.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace CodeZero.Extensions
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class PaginatedListExtensions
    {
        public static PaginatedList<T> ToPaginatedList<T>(this List<T> list, int pageIndex, int pageSize, int total)
        {
            return new PaginatedList<T>(list, pageIndex, pageSize, total);
        }
    }

    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class PaginatedQueryableExtensions
    {
        public static PaginatedList<T> ToPaginatedList<T>(this IQueryable<T> query, int pageIndex, int pageSize, int total)
        {
            var list = query.ToList();
            return new PaginatedList<T>(list, pageIndex, pageSize, total);
        }

        public static IQueryable<T> Paginate<T>(this IQueryable<T> query, int pageIndex, int pageSize)
        {
            var entities = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return entities;
        }
    }
}