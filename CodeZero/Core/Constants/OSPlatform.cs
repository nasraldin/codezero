﻿using System;
using System.ComponentModel;

namespace CodeZero
{
    public static partial class AppConsts
    {
        public enum OSPlatform
        {
            [Description("Windows 10")]
            [StringValue("Windows NT 10")]
            Windows10,

            [Description("Mac OS")]
            [StringValue("Mac OS")]
            MacOS,

            [Description("Mac OS")]
            [StringValue("macOS")]
            macOS,

            [Description("Linux")]
            [StringValue("Linux")]
            Linux,

            [Description("Android")]
            [StringValue("Android")]
            Android,

            [Description("iPad")]
            [StringValue("iPad")]
            iPad,

            [Description("iPhone")]
            [StringValue("iPhone")]
            iPhone,

            [Description("IOS")]
            [StringValue("IOS")]
            IOS,

            [Description("Mobile")]
            [StringValue("Mobile")]
            Mobile,

            [Description("Windows 8.1")]
            [StringValue("Windows NT 6.3")]
            Windows81,

            [Description("Windows 8")]
            [StringValue("Windows NT 6.2")]
            Windows8,

            [Description("Windows 7")]
            [StringValue("Windows NT 6.1")]
            Windows7,

            [Description("Windows Vista")]
            [StringValue("Windows NT 6.0")]
            WindowsVista,

            [Description("Windows XP")]
            [StringValue("Windows NT 5.1")]
            WindowsXP,

            [Description("Windows XP x64")]
            [StringValue("Windows NT 5.2")]
            WindowsXPx64,

            [Description("Windows 2000 SP1")]
            [StringValue("Windows NT 5.01")]
            Windows2000SP1,

            [Description("Windows 2000")]
            [StringValue("Windows NT 5.0")]
            Windows2000,

            [Description("Microsoft Windows NT 4.0")]
            [StringValue("Windows NT 4.0")]
            WindowsNT40,

            [Description("Windows Me")]
            [StringValue("Windows 98; Win 9x 4.90")]
            WindowsMe,

            [Description("Windows Me")]
            [StringValue("Win 9x 4.90")]
            Win9x,

            [Description("Windows 98")]
            [StringValue("Windows 98")]
            Windows98,

            [Description("Windows 95")]
            [StringValue("Windows 95")]
            Windows95,

            [Description("Windows CE")]
            [StringValue("Windows CE")]
            WindowsCE,

            [Description("FreeBSD")]
            [StringValue("FreeBSD")]
            FreeBSD,

            [Description("Windows Phone")]
            [StringValue("Windows Phone")]
            WindowsPhone,

            [Description("Unknown")]
            [StringValue("Unknown")]
            Unknown,
        }
    }
}