﻿namespace CodeZero
{
    public static partial class AppConsts
    {
        public static partial class AuthSchemes
        {
            public const string OAuth2 = "oauth2";
            public const string Bearer = "Bearer";
            public const string ApiKey = "ApiKey";
        }
    }
}