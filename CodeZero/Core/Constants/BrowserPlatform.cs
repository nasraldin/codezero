﻿using System;
using System.ComponentModel;

namespace CodeZero
{
    public static partial class AppConsts
    {
        public enum BrowserPlatform
        {
            [Description("IE")]
            [StringValue("MSIE")]
            IE,

            [Description("Edge")]
            [StringValue("Edg")]
            Edge,

            [Description("Firefox")]
            [StringValue("Firefox")]
            Firefox,

            [Description("Opera")]
            [StringValue("Opera")]
            Opera,

            [Description("Chrome")]
            [StringValue("Chrome")]
            Chrome,

            [Description("Safari")]
            [StringValue("Safari")]
            Safari,

            [Description("Unknown")]
            [StringValue("Unknown")]
            Unknown,
        }
    }
}