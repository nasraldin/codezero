﻿using JetBrains.Annotations;

namespace CodeZero.Tracing
{
    public interface ICorrelationIdProvider
    {
        [NotNull]
        string Get();
    }
}