﻿namespace CodeZero.Tracing
{
    public class CodeZeroCorrelationIdOptions
    {
        public string HttpHeaderName { get; set; } = "X-Correlation-Id";

        public bool SetResponseHeader { get; set; } = true;
    }
}