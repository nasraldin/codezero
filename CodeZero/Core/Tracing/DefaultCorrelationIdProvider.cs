﻿using System;

namespace CodeZero.Tracing
{
    public class DefaultCorrelationIdProvider : ICorrelationIdProvider
    {
        public string Get()
        {
            return CreateNewCorrelationId();
        }

        protected virtual string CreateNewCorrelationId()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}