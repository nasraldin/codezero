﻿using CodeZero;
using dotenv.net;
using System;
using System.IO;

namespace Microsoft.Extensions.Configuration
{
    public static class ConfigurationHelper
    {
        public static IConfigurationRoot BuildConfiguration(
            ConfigurationBuilderOptions options = null,
            Action<IConfigurationBuilder> builderAction = null)
        {
            options ??= new ConfigurationBuilderOptions();

            if (options.BasePath.IsNullOrEmpty())
            {
                options.BasePath = Directory.GetCurrentDirectory();
            }

            if (options.EnvironmentName.IsNullOrEmpty())
            {
                options.EnvironmentName = Environment.GetEnvironmentVariable(AppConsts.ASPNETCORE_ENVIRONMENT);
            }

            Console.WriteLine("[CodeZero] Loads environment variables...");
            var builder = new ConfigurationBuilder()
                .SetBasePath(options.BasePath)
                .AddJsonFile($"{options.FileName}.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"{options.FileName}.{options.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"{options.FileName}.{options.SerilogFileName}.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"{options.FileName}.{options.LanguagesFileName}.json", optional: true, reloadOnChange: true);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("[CodeZero] Environment variables loaded.");
            Console.ResetColor();

            if (options.EnableDotEnvFile)
            {
                Console.WriteLine("[CodeZero] Loads environment variables from a .env...");

                DotEnv.Fluent()
                    .WithExceptions()
                    .WithEnvFiles(".env")
                    .WithTrimValues()
                    .Load();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("[CodeZero] Environment variables loaded from a .env into System.Environment");
                Console.ResetColor();
            }

            if ((options.EnvironmentName == AppConsts.Environments.Development || options.EnvironmentName == AppConsts.Environments.Dev) && options.UserSecretsId != null)
            {
                Console.WriteLine("[CodeZero] Check UserSecrets...");
                builder.AddUserSecrets(options.UserSecretsId);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("[CodeZero] UserSecrets loaded.");
            }

            if (!options.EnvironmentVariablesPrefix.IsNullOrEmpty())
            {
                builder.AddEnvironmentVariables(options.EnvironmentVariablesPrefix);
            }

            if (options.CommandLineArgs != null)
            {
                builder.AddCommandLine(options.CommandLineArgs);
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("[CodeZero] Configuration Build Done!.");
            Console.ResetColor();

            builderAction?.Invoke(builder);

            return builder.Build();
        }
    }
}