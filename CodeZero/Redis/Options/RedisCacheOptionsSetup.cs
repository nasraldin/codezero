using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Options;

namespace CodeZero.Redis.Options
{
    public class RedisCacheOptionsSetup : IConfigureOptions<RedisCacheOptions>
    {
        private readonly IOptions<RedisOptions> _redisOptions;

        public RedisCacheOptionsSetup(IOptions<RedisOptions> redisOptions)
        {
            _redisOptions = redisOptions;
        }

        public void Configure(RedisCacheOptions options)
        {
            options.InstanceName = "ServiceName";
            options.ConfigurationOptions = _redisOptions.Value.ConfigurationOptions;
        }
    }
}