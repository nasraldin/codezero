using StackExchange.Redis;
using System.Threading.Tasks;

namespace CodeZero.Redis
{
    public interface IRedisService
    {
        Task ConnectAsync();
        IConnectionMultiplexer Connection { get; }
        IDatabase Database { get; }
    }
}