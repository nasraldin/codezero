using CodeZero.Caching;
using CodeZero.Locking.Distributed;
using CodeZero.Redis;
using CodeZero.Redis.Options;
using CodeZero.Redis.Services;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Linq;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// </summary>
    public static partial class CodeZeroBuilderExtensions
    {
        /// <summary>
        /// Adds Redis service.
        /// </summary>
        /// <param name="builder">The <see cref="CodeZeroBuilder"/>.</param>
        public static CodeZeroBuilder AddRedis(this CodeZeroBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                try
                {
                    var configurationOptions = ConfigurationOptions.Parse("CodeZero_Redis:Configuration");
                    services.Configure<RedisOptions>(options => options.ConfigurationOptions = configurationOptions);
                }
                catch (Exception)
                {
                    //TODO: add logger
                    //_logger.LogError("'Redis' features are not active on service '{ServiceName}' as the 'Configuration' string is missing or invalid: " + e.Message, "ServiceName");
                    return;
                }

                services.AddSingleton<IRedisService, RedisService>();

                if (services.Any(d => d.ServiceType == typeof(IRedisService)))
                {
                    services.AddStackExchangeRedisCache(o => { });
                    services.AddTransient<IConfigureOptions<RedisCacheOptions>, RedisCacheOptionsSetup>();
                    services.AddScoped<ITagCache, RedisTagCache>();
                    services.AddSingleton<IDistributedLock, RedisLock>();
                    services.AddTransient<IConfigureOptions<KeyManagementOptions>, RedisKeyManagementOptionsSetup>();
                }
            });

            return builder;
        }
    }
}