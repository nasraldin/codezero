﻿using Autofac;
using AutofacSerilogIntegration;
using MediatR;
using MediatR.Pipeline;
using System;
using System.Collections.Generic;
using System.Reflection;
using Module = Autofac.Module;

namespace CodeZero.Autofac
{
    public class AutofacModule : Module
    {
        private readonly string _env = Environment.GetEnvironmentVariable(AppConsts.ASPNETCORE_ENVIRONMENT);
        private readonly List<Assembly> _assemblies = new();

        public AutofacModule(Assembly callingAssembly = null)
        {
            var coreAssembly = Assembly.GetAssembly(typeof(Startup));
            //var infrastructureAssembly = Assembly.GetAssembly(typeof(ShowAllServicesMiddleware));

            _assemblies.Add(coreAssembly);
            //_assemblies.Add(infrastructureAssembly);
            if (callingAssembly != null)
            {
                _assemblies.Add(callingAssembly);
            }
        }

        protected override void Load(ContainerBuilder builder)
        {
            if (_env == AppConsts.Environments.Development || _env == AppConsts.Environments.Dev)
            {
                RegisterDevelopmentOnlyDependencies(builder);
            }
            else
            {
                RegisterProductionOnlyDependencies(builder);
            }
            RegisterCommonDependencies(builder);
        }

        private void RegisterCommonDependencies(ContainerBuilder builder)
        {
            // use non-generic ILogger
            builder.RegisterLogger();

            builder.RegisterType<Startup>().As<IStartup>().InstancePerLifetimeScope();
            //builder.RegisterType<PoweredByMiddlewareOptions>().As<IPoweredByMiddlewareOptions>().InstancePerLifetimeScope();
            //builder.RegisterType<EfRepository>().As<IRepository>().InstancePerLifetimeScope();
            //builder.RegisterType<EmailSender>().As<IEmailSender>().InstancePerLifetimeScope();
            //builder.RegisterGeneric(typeof(Config<>)).As(typeof(IConfig<>)).InstancePerDependency();

            builder
                .RegisterType<Mediator>()
                .As<IMediator>()
                .InstancePerLifetimeScope();

            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            var mediatrOpenTypes = new[]
            {
                typeof(IRequestHandler<,>),
                typeof(IRequestExceptionHandler<,,>),
                typeof(IRequestExceptionAction<,>),
                typeof(INotificationHandler<>),
            };

            foreach (var mediatrOpenType in mediatrOpenTypes)
            {
                builder
                .RegisterAssemblyTypes(_assemblies.ToArray())
                .AsClosedTypesOf(mediatrOpenType)
                .AsImplementedInterfaces();
            }
        }

        private void RegisterDevelopmentOnlyDependencies(ContainerBuilder builder)
        {
            // TODO: Add development only services
        }

        private void RegisterProductionOnlyDependencies(ContainerBuilder builder)
        {
            // TODO: Add production only services
        }
    }
}