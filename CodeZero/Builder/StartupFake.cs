using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CodeZero
{
    /// <summary>
    /// Represents a fake Startup class that is composed of Configure and ConfigureServices lambdas.
    /// </summary>
    internal class StartupFake : StartupBase
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly StartupActions _actions;

        public StartupFake(IServiceProvider serviceProvider, StartupActions actions, int order)
        {
            _serviceProvider = serviceProvider;
            _actions = actions;
            Order = order;
        }

        /// <inheritdoc />
        public override int Order { get; }

        /// <inheritdoc />
        public override void ConfigureServices(IServiceCollection services)
        {
            foreach (var configureServices in _actions.ConfigureServicesActions)
            {
                configureServices?.Invoke(services, _serviceProvider);
            }
        }

        /// <inheritdoc />
        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            foreach (var configure in _actions.ConfigureActions)
            {
                configure?.Invoke(app, routes, serviceProvider);
            }
        }
    }
}