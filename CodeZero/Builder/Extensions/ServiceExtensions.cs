using CodeZero;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Adds CodeZeroCore services to the application.
        /// </summary>
        public static CodeZeroBuilder AddCodeZeroCore(this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));

            var builder = services.AddCodeZero().AddRegisterStartup();

            return builder;
        }

        /// <summary>
        /// Adds CodeZeroCore services to the application and let the app change the
        /// default tenant behavior and set of features through a configure action.
        /// </summary>
        public static IServiceCollection AddCodeZeroCore(this IServiceCollection services, Action<CodeZeroBuilder> configure)
        {
            var builder = services.AddCodeZeroCore();

            configure?.Invoke(builder);

            return services;
        }

        /// <summary>
        /// Adds Register Startup Configurations.
        /// </summary>
        public static CodeZeroBuilder AddRegisterStartup(this CodeZeroBuilder builder)
        {
            builder.ConfigureServices(collection =>
            {
                // Allows a tenant to add its own route endpoint schemes for link generation.
            },
            // Need to be registered last.
            order: int.MaxValue - 100);

            return builder.RegisterStartup<Startup>();
        }
    }
}