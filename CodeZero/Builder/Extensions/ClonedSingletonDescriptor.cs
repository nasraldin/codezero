using Microsoft.Extensions.DependencyInjection;
using System;

namespace CodeZero.Builder
{
    public class ClonedSingletonDescriptor : ServiceDescriptor
    {
        public ClonedSingletonDescriptor(ServiceDescriptor parent, object implementationInstance)
            : base(parent.ServiceType, implementationInstance)
        {
            Parent = parent;
        }

        public ClonedSingletonDescriptor(ServiceDescriptor parent, Func<IServiceProvider, object> implementationFactory)
            : base(parent.ServiceType, implementationFactory, ServiceLifetime.Singleton)
        {
            Parent = parent;
        }

        public ServiceDescriptor Parent { get; }
    }
}