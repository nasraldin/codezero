using CodeZero;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;

namespace Microsoft.Extensions.DependencyInjection
{
    public class CodeZeroBuilder
    {
        private Dictionary<int, StartupActions> Actions { get; } = new Dictionary<int, StartupActions>();

        public IServiceCollection ApplicationServices { get; }

        public CodeZeroBuilder(IServiceCollection services)
        {
            ApplicationServices = services;
        }

        public CodeZeroBuilder RegisterStartup<T>() where T : class, IStartup
        {
            ApplicationServices.AddTransient<IStartup, T>();

            return this;
        }

        /// <summary>
        /// This method gets called for each service. Use this method to add services to the container.
        /// For more information on how to configure your application, 
        /// visit <see href="http://go.microsoft.com/fwlink/?LinkID=398940">The Startup class</see>
        /// </summary>
        /// <param name="configure">The action to execute when configuring the services for a service.</param>
        /// <param name="order">The order of the action to execute. Lower values will be executed first.</param>
        public CodeZeroBuilder ConfigureServices(Action<IServiceCollection, IServiceProvider> configure, int order = 0)
        {
            if (!Actions.TryGetValue(order, out var actions))
            {
                actions = Actions[order] = new StartupActions(order);

                ApplicationServices.AddTransient<IStartup>(sp => new StartupFake(sp.GetRequiredService<IServiceProvider>(), actions, order));
            }

            actions.ConfigureServicesActions.Add(configure);

            return this;
        }

        /// <summary>
        /// This method gets called for each service. Use this method to add services to the container.
        /// For more information on how to configure your application, 
        /// visit <see href="http://go.microsoft.com/fwlink/?LinkID=398940">The Startup class</see>
        /// </summary>
        /// <param name="configure">The action to execute when configuring the services for a service.</param>
        /// <param name="order">The order of the action to execute. Lower values will be executed first.</param>
        public CodeZeroBuilder ConfigureServices(Action<IServiceCollection> configure, int order = 0)
        {
            return ConfigureServices((s, sp) => configure(s), order);
        }

        /// <summary>
        /// This method gets called for each service. Use this method to configure the request's pipeline.
        /// </summary>
        /// <param name="configure">The action to execute when configuring the request's pipeline for a service.</param>
        /// <param name="order">The order of the action to execute. Lower values will be executed first.</param>
        public CodeZeroBuilder Configure(Action<IApplicationBuilder, IEndpointRouteBuilder, IServiceProvider> configure, int order = 0)
        {
            if (!Actions.TryGetValue(order, out var actions))
            {
                actions = Actions[order] = new StartupActions(order);

                ApplicationServices.AddTransient<IStartup>(sp => new StartupFake(sp.GetRequiredService<IServiceProvider>(), actions, order));
            }

            actions.ConfigureActions.Add(configure);

            return this;
        }

        /// <summary>
        /// This method gets called for each service. Use this method to configure the request's pipeline.
        /// </summary>
        /// <param name="configure">The action to execute when configuring the request's pipeline for a service.</param>
        /// <param name="order">The order of the action to execute. Lower values will be executed first.</param>
        public CodeZeroBuilder Configure(Action<IApplicationBuilder, IEndpointRouteBuilder> configure, int order = 0)
        {
            return Configure((app, routes, sp) => configure(app, routes), order);
        }

        /// <summary>
        /// This method gets called for each service. Use this method to configure the request's pipeline.
        /// </summary>
        /// <param name="configure">The action to execute when configuring the request's pipeline for a service.</param>
        /// <param name="order">The order of the action to execute. Lower values will be executed first.</param>
        public CodeZeroBuilder Configure(Action<IApplicationBuilder> configure, int order = 0)
        {
            return Configure((app, routes, sp) => configure(app), order);
        }
    }
}