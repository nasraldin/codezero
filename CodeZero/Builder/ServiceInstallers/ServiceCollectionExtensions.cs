using CodeZero;
using CodeZero.Configuration;
using CodeZero.Domain.Common.Behaviours;
using CodeZero.Email;
using CodeZero.Helpers;
using CodeZero.Locking;
using CodeZero.Locking.Distributed;
using CodeZero.Middleware;
using CodeZero.Timing;
using JetBrains.Annotations;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.FeatureManagement;
using Serilog;
using System;
using System.Linq;
using System.Reflection;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Adds CodeZero services to the host service collection
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds CodeZero services to the host service collection.
        /// </summary>
        public static CodeZeroBuilder AddCodeZero([NotNull] this IServiceCollection services)
        {
            // If an instance of CodeZeroBuilder exists reuse it,
            // so we can call AddCodeZero several times.
            if (services
               .LastOrDefault(d => d.ServiceType == typeof(CodeZeroBuilder))?
               .ImplementationInstance is not CodeZeroBuilder builder)
            {
                builder = new CodeZeroBuilder(services);
                services.AddSingleton(builder);

                // Register the list of default services
                AddDefaultServices(builder);
                //AddLocalizationServices(builder);

                // Register the list of services to be resolved later on
                services.AddSingleton(services);
            }

            return builder;
        }

        /// <summary>
        /// Adds CodeZero services to the host service collection and let the app change
        /// the default behavior and set of features through a configure action.
        /// </summary>
        public static IServiceCollection AddCodeZero(this IServiceCollection services,
            Action<CodeZeroBuilder> configure)
        {
            var builder = services.AddCodeZero();

            configure?.Invoke(builder);

            return services;
        }

        private static void AddDefaultServices(CodeZeroBuilder builder)
        {
            Console.WriteLine("[CodeZero] Adds CodeZero services to the host service collection...");

            // Register startup ApplicationServices
            var services = builder.ApplicationServices;

            // Add Load configuration from appsettings.json
            services.AddOptions();

            var Configuration = services.BuildServiceProvider().GetRequiredService<IConfiguration>();
            var WebHostEnv = services.BuildServiceProvider().GetRequiredService<IWebHostEnvironment>();

            // Register FeatureManager
            services.AddFeatureManagement(Configuration);
            var featureManager = services.BuildServiceProvider().GetRequiredService<IFeatureManager>();

            // Load environment variable & core configuration
            var env = Environment.GetEnvironmentVariable(AppConsts.ASPNETCORE_ENVIRONMENT);
            var coreConfig = new CoreConfiguration(Configuration);
            var appSettings = new AppSettings(Configuration, WebHostEnv, featureManager);

            // Register Core configuration
            services.AddTransient(typeof(ICoreConfiguration), typeof(CoreConfiguration));
            services.TryAddSingleton<ICoreConfiguration>(CoreConfiguration.Instance);
            services.AddTransient(typeof(IConfig<>), typeof(Config<>));
            services.AddTransient(typeof(IAppSettings), typeof(AppSettings));
            services.TryAddSingleton<IAppSettings>(AppSettings.Instance);

            // Disabled feature filter hasn't been registered
            services.Configure<FeatureManagementOptions>(options =>
            {
                options.IgnoreMissingFeatureFilters = coreConfig.ApiSettings.IgnoreMissingFeatureFilters;
            });

            // Use non-generic Serilog.ILogger
            services.AddSingleton(Log.Logger);

            // Register default framework order
            services.AddLocalizationServices();

            if (coreConfig.FeatureManagement.EnableMemoryCache)
                services.AddMemoryCache();

            services.AddWebEncoders();
            services.AddHttpContextAccessor();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IClock, Clock>();
            services.AddScoped<ILocalClock, LocalClock>();
            services.AddSingleton<IPoweredByMiddlewareOptions, PoweredByMiddlewareOptions>();

            builder.ConfigureServices(s =>
            {
                s.AddSingleton<LocalLock>();
                s.AddSingleton<ILocalLock>(sp => sp.GetRequiredService<LocalLock>());
                s.AddSingleton<IDistributedLock>(sp => sp.GetRequiredService<LocalLock>());
            });

            if (coreConfig.ProxySettings.EnableProxy)
                services.AddProxy();

            if (coreConfig.ApiSettings.EnableResponseCompression)
                services.AddResponseCompressionConfig();

            if (coreConfig.IpRateLimiting.EnableEndpointRateLimiting)
                services.AddRateLimitingClientIP(Configuration);

            services.AddAntiforgeryConfig();

            //if (coreConfig.FeatureManagement.Authentication)
            //    services.AddAuth(Configuration);

            if (coreConfig.CorsSettings.EnableCors)
                services.AddCorsConfig();

            if (coreConfig.FeatureManagement.EnableSwagger)
            {
                services.AddCodeZeroApiVersioning();
                services.AddSwaggerVersioned();
            }

            //Todo:
            // Add MediatR
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));

            // Adds e-mail address validator service.
            services.AddTransient<IEmailAddressValidator, EmailAddressValidator>();

            services.AddDataProtectionConfig();

            var httpContextAccessor = services.BuildServiceProvider().GetRequiredService<IHttpContextAccessor>();
            var httpContextHelper = new HttpContextHelper(httpContextAccessor);
            services.AddTransient(typeof(IHttpContextHelper), typeof(HttpContextHelper));
            services.TryAddSingleton<IHttpContextHelper>(HttpContextHelper.Current);

            services.AddMvcServices();
        }
    }
}