using CodeZero.Configuration;
using CodeZero.Middleware;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Configure ApplicationBuilder.
    /// </summary>
    public static partial class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Configure HTTP request pipeline for the current path.
        /// </summary>
        public static IApplicationBuilder UseCodeZero(this IApplicationBuilder app,
            Action<IApplicationBuilder> configure = null)
        {
            // Adds middleware for streamlined request logging.
            app.UseSerilogRequestLogging();
            app.UseMiddleware<PoweredByMiddleware>();

            if (CoreConfiguration.Instance.ProxySettings.EnableProxy)
                app.UseProxy();

            if (CoreConfiguration.Instance.ApiSettings.EnableResponseCompression)
                app.UseResponseCompression();

            if (CoreConfiguration.Instance.IpRateLimiting.EnableEndpointRateLimiting)
                app.UseRateLimitingClientIP();

            app.UseLocalizationServices();
            app.UseStaticFiles();

            app.UseHeaders();

            if (CoreConfiguration.Instance.FeatureManagement.EnableSwagger)
            {
                // Enable middleware to serve generated Swagger with wersioning as a JSON endpoint.
                var provider = app.ApplicationServices
                    .GetRequiredService<IApiVersionDescriptionProvider>();
                app.UseSwaggerVersioned(provider);
            }

            app.UseRouting();

            if (CoreConfiguration.Instance.CorsSettings.EnableCors)
                app.UseCorsConfig();

            if (CoreConfiguration.Instance.FeatureManagement.Authentication || CoreConfiguration.Instance.ApiKeyConfig.EnableApiKey)
                app.UseAuth();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            configure?.Invoke(app);

            return app;
        }
    }
}