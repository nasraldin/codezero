﻿using Autofac.Extensions.DependencyInjection;
using CodeZero;
using CodeZero.Configuration;
using CodeZero.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace Microsoft.Extensions.Hosting
{
    /// <summary>
    /// CodeZero Host Builder: A program initialization configuration parameters.
    /// </summary>
    public static class CodeZeroHostBuilder
    {
        public static IHostBuilder CreateHostBuilder<Startup>(string[] args = null, ConfigurationBuilderOptions options = null) where Startup : class
        {
            options ??= new ConfigurationBuilderOptions
            {
                BasePath = Directory.GetCurrentDirectory(),
                EnvironmentName = Environment.GetEnvironmentVariable(AppConsts.ASPNETCORE_ENVIRONMENT),
                CommandLineArgs = args
            };

            return CreateHostBuilder<Startup>(args, BuildConfiguration(options));
        }

        public static IHostBuilder CreateHostBuilder<Startup>(string[] args, IConfiguration configuration) where Startup : class
        {
            var feature = configuration.GetSection(nameof(FeatureManagement)).Get<CodeZero.Configuration.FeatureManagement>();
            var debugConfig = configuration.GetSection(nameof(DebugConfig)).Get<DebugConfig>();

            // Host builder
            var host = Host.CreateDefaultBuilder(args);

            if (feature.UseAutofac)
                host.UseServiceProviderFactory(new AutofacServiceProviderFactory());

            host.UseSerilog()
                .ConfigureWebHostDefaults(webHostBuilder =>
                {
                    webHostBuilder
                    .UseKestrel(c => c.AddServerHeader = false)
                    .UseConfiguration(configuration)
                    .CaptureStartupErrors(debugConfig.CaptureStartupErrors)
                    .UseStartup<Startup>()
                    .UseSetting(WebHostDefaults.DetailedErrorsKey, debugConfig.DetailedErrorsKey.ToString().ToLower());
                });

            // Add Serilog configuration
            SerilogConfig.SetupLogger(configuration);
            Console.ResetColor();

            return host;
        }

        public static IConfiguration BuildConfiguration(ConfigurationBuilderOptions options)
        {
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Assembly.GetExecutingAssembly().FullName.Replace("Culture=neutral, PublicKeyToken=null", "Nasr Aldin"));
            Console.ResetColor();
            Thread.CurrentThread.Name = $"{AppDomain.CurrentDomain.FriendlyName} Main thread";
            Console.WriteLine($"[CodeZero] CurrentThread: {Thread.CurrentThread.Name}");
            Console.WriteLine("[CodeZero] Build configuration...");

            if (!File.Exists(Path.Combine(Directory.GetCurrentDirectory(), Path.GetFileName($"{options.FileName}.{options.EnvironmentName}.json"))))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{options.FileName}.{options.EnvironmentName}.json FileNotFound");
                throw new FileNotFoundException($"{options.FileName}.{options.EnvironmentName}.json file is not existing. " +
                    $"Please check your {options.FileName}.{options.EnvironmentName}.json is exist or create new one from appsettings.Template.json");
            }

            return ConfigurationHelper.BuildConfiguration(options);
        }
    }
}