﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Exceptions;
using Serilog.Exceptions.Core;
using Serilog.Exceptions.EntityFrameworkCore.Destructurers;
using System;
using System.Diagnostics;

namespace CodeZero.Logging
{
    public class SerilogConfig
    {
        public static void SetupLogger(IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails(new DestructuringOptionsBuilder()
                .WithDefaultDestructurers()
                .WithDestructurers(new[] { new DbUpdateExceptionDestructurer() }))
                //.Enrich.WithProperty("ApplicationName", typeof(Program).Assembly.GetName().Name)
                .Enrich.WithProperty("Environment", Environment.GetEnvironmentVariable(AppConsts.ASPNETCORE_ENVIRONMENT))
                // Used to filter out potentially bad data due debugging.
                // Very useful when doing Seq dashboards and want to remove logs under debugging session.
#if DEBUG
                .Enrich.WithProperty("DebuggerAttached", Debugger.IsAttached)
#endif
                .CreateLogger();

            // get ApplicationName => typeof(Program).Assembly.GetName().Name
            // sqlserver
            // Serilog.Exceptions.SqlServer Serilog.Exceptions.MsSqlServer
            //.Enrich.WithExceptionDetails(new DestructuringOptionsBuilder()
            //                       .WithDefaultDestructurers()
            //                       .WithDestructurers(new[] { new SqlExceptionDestructurer() }))

            // EntityFrameworkCore
            // Serilog.Exceptions.EntityFrameworkCore
            //.Enrich.WithExceptionDetails(new DestructuringOptionsBuilder()
            //                       .WithDefaultDestructurers()
            //                       .WithDestructurers(new[] { new DbUpdateExceptionDestructurer() }))
        }
    }
}