﻿using CodeZero.Domain.Messaging;
using System.Threading.Tasks;

namespace CodeZero.Domain
{
    public interface IDomainEventDispatcher
    {
        Task Dispatch(Event domainEvent);
    }
}