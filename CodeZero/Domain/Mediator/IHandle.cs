﻿using CodeZero.Domain.Messaging;
using System.Threading.Tasks;

namespace CodeZero.Domain.Mediator
{
    public interface IHandle<in T> where T : Event
    {
        Task Handle(T domainEvent);
    }
}