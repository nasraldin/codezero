using CodeZero;
using CodeZero.Domain.Builders;
using JetBrains.Annotations;
using Scrutor;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add Domain Builders
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddDomainBuilders([NotNull] this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));

            return services.Scan(selector => selector.FromAssemblies(Application.Assemblies)
                    .AddClasses(filter => filter.AssignableTo(typeof(IBuilder<,,>)))
                    .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                    .AsImplementedInterfaces()
                    .WithScopedLifetime());
        }
    }
}