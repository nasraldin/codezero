﻿using CodeZero.Domain.Messaging;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CodeZero.Domain.Entities
{
    /// <summary>
    /// Defines an entity. It's primary key may not be "Id" or it may have a composite primary key.
    /// Use <see cref="IEntity{TKey}"/> where possible for better integration to repositories and other structures in the framework.
    /// </summary>
    [Serializable]
    public abstract class Entity : IEntity
    {
        /// <inheritdoc/>
        public override string ToString()
        {
            return $"[ENTITY: {GetType().Name}] Keys = {GetKeys().JoinAsString(", ")}";
        }

        public abstract object[] GetKeys();

        public bool EntityEquals(IEntity other)
        {
            return EntityHelper.EntityEquals(this, other);
        }
    }

    /// <summary>
    /// Basic implementation of IEntity interface.
    /// An entity can inherit this class of directly implement to IEntity interface.
    /// </summary>
    /// <typeparam name="TKey">Type of the primary key of the entity</typeparam>
    [Serializable]
    public abstract class BaseEntity<TKey> : Entity, IEntity<TKey>
    {
        /// <inheritdoc />
        public virtual TKey Id { get; protected init; }

        protected BaseEntity() { }

        protected BaseEntity(TKey id) => Id = id;

        #region DomainEvents

        private List<Event> _domainEvents;
        public IReadOnlyCollection<Event> DomainEvents => _domainEvents?.AsReadOnly();

        public void AddDomainEvent(Event domainEvent)
        {
            _domainEvents ??= new List<Event>();
            _domainEvents.Add(domainEvent);
        }

        public void RemoverEvento(Event domainEvent)
        {
            _domainEvents?.Remove(domainEvent);
        }

        public void ClearDomainEvents()
        {
            _domainEvents?.Clear();
        }

        #endregion

        #region BaseBehaviours

        /// <inheritdoc />
        public virtual bool IsTransient()
        {
            if (EqualityComparer<TKey>.Default.Equals(Id, default))
            {
                return true;
            }

            // Workaround for EF Core since it sets int / long to min value when attaching to dbcontext
            if (typeof(TKey) == typeof(int))
            {
                return Convert.ToInt32(Id) <= 0;
            }

            if (typeof(TKey) == typeof(long))
            {
                return Convert.ToInt64(Id) <= 0;
            }

            return false;
        }

        private static bool IsTransient(BaseEntity<TKey> obj)
        {
            return obj != null && Equals(obj.Id, default(TKey));
        }

        private Type GetUnproxiedType()
        {
            return GetType();
        }

        //public override bool Equals(object obj)
        //{
        //    var compareTo = obj as BaseEntity<TKey>;

        //    if (ReferenceEquals(this, compareTo)) return true;
        //    if (ReferenceEquals(null, compareTo)) return false;

        //    return Id.Equals(compareTo.Id);
        //}

        public override bool Equals(object obj)
        {
            return Equals(obj as BaseEntity<TKey>);
        }

        public virtual bool Equals(BaseEntity<TKey> other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (IsTransient(this) || IsTransient(other) || !Equals(Id, other.Id)) return false;

            var otherType = other.GetUnproxiedType();
            var thisType = GetUnproxiedType();
            return thisType.IsAssignableFrom(otherType) || otherType.IsAssignableFrom(thisType);
        }

        public override int GetHashCode()
        {
            if (Equals(Id, default(int)))
                return base.GetHashCode();
            return Id.GetHashCode();
        }

        //public override int GetHashCode()
        //{
        //    return (GetType().GetHashCode() ^ 93) + Id.GetHashCode();
        //}

        //public static bool operator ==(Entity a, Entity b)
        //{
        //    if (a is null && b is null)
        //        return true;

        //    if (a is null || b is null)
        //        return false;

        //    return a.Equals(b);
        //}

        public static bool operator ==(BaseEntity<TKey> x, BaseEntity<TKey> y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(BaseEntity<TKey> x, BaseEntity<TKey> y)
        {
            return !(x == y);
        }

        public override object[] GetKeys()
        {
            return new object[] { Id };
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"[ENTITY: {GetType().Name}] Id = {Id}";
        }

        #endregion

        #region Validation

        [NotMapped]
        public bool IsValid => ValidationResult?.IsValid ?? Validate();

        [NotMapped]
        public ValidationResult ValidationResult { get; private set; }

        protected bool OnValidate<TValidator, TEntity>(TEntity entity, TValidator validator)
            where TValidator : AbstractValidator<TEntity>
            where TEntity : BaseEntity<TKey>
        {
            ValidationResult = validator.Validate(entity);
            return IsValid;
        }

        protected bool OnValidate<TValidator, TEntity>(TEntity entity, TValidator validator,
            Func<AbstractValidator<TEntity>, TEntity, ValidationResult> validation)
            where TValidator : AbstractValidator<TEntity>
            where TEntity : BaseEntity<TKey>
        {
            ValidationResult = validation(validator, entity);
            return IsValid;
        }

        protected void AddError(string errorMessage, ValidationResult validationResult = default)
        {
            ValidationResult.Errors.Add(new(default, errorMessage));
            validationResult?.Errors.ToList().ForEach(failure => ValidationResult.Errors.Add(failure));
        }

        protected abstract bool Validate();

        #endregion
    }
}