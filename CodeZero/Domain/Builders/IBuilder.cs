﻿using CodeZero.Domain.Entities;
using System;

namespace CodeZero.Domain.Builders
{
    public interface IBuilder<out TBuilder, out TEntity, TKey>
        where TBuilder : IBuilder<TBuilder, TEntity, TKey>
        where TEntity : BaseEntity<TKey>
        where TKey : struct
    {
        TEntity Build();
        TBuilder With<TProperty>(Func<TBuilder, TProperty> func);
    }
}