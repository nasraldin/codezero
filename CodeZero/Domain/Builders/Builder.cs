﻿using CodeZero.Domain.Builders.Extensions;
using CodeZero.Domain.Entities;
using System;

namespace CodeZero.Domain.Builders
{
    public abstract class Builder<TBuilder, TEntity, TKey> : IBuilder<TBuilder, TEntity, TKey>
        where TBuilder : Builder<TBuilder, TEntity, TKey>
        where TEntity : BaseEntity<TKey>
        where TKey : struct
    {
        protected TKey Id;
        public abstract TEntity Build();

        public TBuilder With<TProperty>(Func<TBuilder, TProperty> func) => ((TBuilder)this).With<TBuilder, TProperty>(func);
    }
}