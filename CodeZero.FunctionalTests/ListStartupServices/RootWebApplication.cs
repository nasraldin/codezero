using CodeZero.FunctionalTests.ListStartupServices;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace CodeZero.UnitTests.ListStartupServices
{
    public class RootWebApplication : IClassFixture<CustomWebApplicationFactory<WebApp.Startup>>
    {
        private readonly HttpClient _client;

        public RootWebApplication(CustomWebApplicationFactory<WebApp.Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task ReturnDefaultContent()
        {
            HttpResponseMessage response = await _client.GetAsync("/");
            response.EnsureSuccessStatusCode();
            string stringResponse = await response.Content.ReadAsStringAsync();

            Assert.Contains("Default content", stringResponse);
        }
    }
}
