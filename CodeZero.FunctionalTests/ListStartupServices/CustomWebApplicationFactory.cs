using CodeZero.ListStartupServices;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace CodeZero.FunctionalTests.ListStartupServices
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<WebApp.Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                // Create a new service provider.
                var serviceProvider = new ServiceCollection()
                .BuildServiceProvider();

                services.Configure<ServiceConfig>(config =>
                {
                    config.Services = new List<ServiceDescriptor>(services);
                    config.Path = "/mytestservices";
                });
            });
        }
    }
}
