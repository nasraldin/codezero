﻿using CodeZero.Result;
using CodeZero.UnitTests.Result.Sample.DTOs;
using CodeZero.UnitTests.Result.Sample.Model;
using CodeZero.UnitTests.Result.Sample.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace CodeZero.WebApp.WeatherForecastFeature
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly WeatherService _weatherService;
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(WeatherService weatherService, ILogger<WeatherForecastController> logger)
        {
            _weatherService = weatherService;
            _logger = logger;
        }

        /// <summary>
        /// This uses a filter to convert an CodeZero.Result return type to an ActionResult.
        /// This filter could be used per controller or globally!
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [TranslateResultToActionResult]
        [HttpPost("Create")]
        public Result<IEnumerable<WeatherForecast>> CreateForecast([FromBody] ForecastRequestDto model)
        {
            return _weatherService.GetForecast(model);
        }
    }
}