﻿//using CodeZero.UnitTests.Result.Sample.DTOs;
//using CodeZero.UnitTests.Result.Sample.Model;
//using CodeZero.UnitTests.Result.Sample.Services;
//using Microsoft.AspNetCore.Mvc;
//using System.Collections.Generic;

//namespace CodeZero.WebApp.WeatherForecastFeature
//{
//    public class ForecastEndpoint : BaseEndpoint<ForecastRequestDto, IEnumerable<WeatherForecast>>
//    {
//        private readonly WeatherService _weatherService;

//        public ForecastEndpoint(WeatherService weatherService)
//        {
//            _weatherService = weatherService;
//        }

//        /// <summary>
//        /// This uses an extension method to convert to an ActionResult
//        /// </summary>
//        /// <param name="model"></param>
//        /// <returns></returns>
//        [HttpPost("/Forecast/New")]
//        public override ActionResult<IEnumerable<WeatherForecast>> Handle(ForecastRequestDto request)
//        {
//            return this.ToActionResult(_weatherService.GetForecast(request));
//        }
//    }
//}