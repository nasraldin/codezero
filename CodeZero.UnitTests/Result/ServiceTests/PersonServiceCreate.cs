using CodeZero.Result;
using CodeZero.UnitTests.Result.Sample.Services;
using FluentAssertions;
using Xunit;

namespace CodeZero.UnitTests.Result.ServiceTests
{
    public class PersonServiceCreate
    {
        [Fact]
        public void ReturnsInvalidResultGivenEmptyNames()
        {
            var service = new PersonService();

            var result = service.Create("", "");

            result.Status.Should().Be(ResultStatus.Invalid);
            result.ValidationErrors.Count.Should().Be(2);
        }

        [Fact]
        public void ReturnsInvalidResultWith2ErrorsGivenSomeLongNameSurname()
        {
            var service = new PersonService();

            var result = service.Create("Steve", "SomeLongName");

            result.Status.Should().Be(ResultStatus.Invalid);
            result.ValidationErrors.Count.Should().Be(2);
        }

    }
}