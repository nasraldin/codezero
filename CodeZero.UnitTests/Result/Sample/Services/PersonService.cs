﻿using CodeZero.Result;
using CodeZero.UnitTests.Result.Sample.Model;
using CodeZero.UnitTests.Result.Sample.Validators;
using FluentValidation.Results;

namespace CodeZero.UnitTests.Result.Sample.Services
{
    public class PersonService
    {
        public Result<Person> Create(string firstName, string lastName)
        {
            var person = new Person();
            person.Forename = firstName;
            person.Surname = lastName;

            var validator = new PersonValidator();

            var result = validator.Validate(person);
            if (!result.IsValid)
            {
                return Result<Person>.Invalid(result.AsErrors());
            }

            return Result<Person>.Success(person);
        }
    }
}