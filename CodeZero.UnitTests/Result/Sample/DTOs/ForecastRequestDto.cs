﻿using System.ComponentModel.DataAnnotations;

namespace CodeZero.UnitTests.Result.Sample.DTOs
{
    public class ForecastRequestDto
    {
        [Required]
        public string PostalCode { get; set; }
    }
}